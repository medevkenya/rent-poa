<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class Propertytypes extends Model
{

    protected $table = 'propertytypes';

    public static function getAll($categoryId) {
      return Propertytypes::where('categoryId',$categoryId)->where('is_deleted',0)->get();
    }

    public static function getByCategoryId($categoryId) {
      return Propertytypes::where('categoryId',$categoryId)->where('is_deleted',0)->get();
    }

    public static function getByCategoryIdAll($categoryId) {
      return Propertytypes::leftJoin('properties','propertytypes.id','=','properties.propertyTypeId')
           ->selectRaw('propertytypes.*, count(properties.propertyTypeId) AS `count`')
           ->where('propertytypes.categoryId',$categoryId)
           ->where('propertytypes.is_deleted',0)
           ->groupBy('propertytypes.id')
           ->orderBy('count','DESC')
           ->limit(10)
           ->get();
    }

}
