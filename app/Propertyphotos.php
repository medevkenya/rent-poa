<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class Propertyphotos extends Model
{

    protected $table = 'propertyphotos';

    public static function savePhoto($propertyId,$photo) {
      $model = new Propertyphotos;
      $model->propertyId = $propertyId;
      $model->photo = $photo;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

    public static function removePhoto($id) {
        $model = Propertyphotos::find($id);
        $model->is_deleted = 1;
        $model->save();
        if ($model) {
          return true;
        }
        else {
          return false;
        }
    }

    public static function getPropertyPhotos($propertyId) {
      $data = Propertyphotos::where('propertyId',$propertyId)->get();
      if(count($data) < 1) {
        return false;
      }
      else {
        return $data;
      }
    }

}
