<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class Propertycategories extends Model
{

    protected $table = 'propertycategories';

    public static function getAll() {
      return Propertycategories::where('is_deleted',0)->get();
    }

    public static function getAllAll() {
      return Propertycategories::leftJoin('properties','propertycategories.id','=','properties.propertyTypeId')
           ->selectRaw('propertycategories.*, count(properties.propertyTypeId) AS `count`')
           ->where('propertycategories.is_deleted',0)
           ->groupBy('propertycategories.id')
           ->orderBy('count','DESC')
           ->get();
    }

}
