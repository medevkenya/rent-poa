<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class Propertysection extends Model
{

    protected $table = 'propertysection';

    public static function getAll() {
      return Propertysection::where('is_deleted',0)->get();
    }

}
