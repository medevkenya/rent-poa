<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class Counties extends Model
{

    protected $table = 'counties';

    public static function getAll() {
      return Counties::where('is_deleted',0)->get();
    }

}
