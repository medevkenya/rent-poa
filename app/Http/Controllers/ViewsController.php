<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use Auth;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Properties;
use App\Propertytypes;
use App\Subcounties;
use App\Propertycategories;
use App\Propertyphotos;

class ViewsController extends Controller
{

    public function viewByLocation($subCountyId)
    {
       $locationDetails = Subcounties::where('id',$subCountyId)->first();
       $data = Properties::getPropertiesByLocation($subCountyId);
       return view('frontend.viewbylocation',['data'=>$data,'locationDetails'=>$locationDetails]);
    }

    public function viewByCategory($categoryId)
    {
       $categoryDetails = Propertycategories::where('id',$categoryId)->first();
       if(!$categoryDetails) {
         return back()->with('error', 'Failed to get property');
       }
       $data = Properties::getPropertiesByCategory($categoryId);
       return view('frontend.viewbycategory',['data'=>$data,'categoryDetails'=>$categoryDetails]);
    }

    public function viewByType($typeId)
    {
       $typeDetails = Propertytypes::where('id',$typeId)->first();
       $data = Properties::getPropertiesByType($typeId);
       return view('frontend.viewbytype',['data'=>$data,'typeDetails'=>$typeDetails]);
    }

    public function viewRent($propertyId)
    {
       $propertyDetails = Properties::getPropertyById($propertyId);
       $propertyPhotos = Propertyphotos::getPropertyPhotos($propertyId);
       if(!$propertyPhotos) {
         return back()->with('error', 'Failed to get property photos');
       }
       Properties::updateViews($propertyId);
       return view('frontend.viewrent',['propertyDetails'=>$propertyDetails,'propertyPhotos'=>$propertyPhotos]);
    }

}
