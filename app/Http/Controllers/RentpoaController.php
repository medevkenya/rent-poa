<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use Mail;
use Hash;
use Auth;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Properties;
use App\Propertytypes;
use App\Subcounties;
use App\Propertyphotos;
use App\Customers;
use App\Propertycategories;
use Intervention\Image\Facades\Image as Image;
use Log;

class RentpoaController extends Controller
{

    public function rentpoa()
    {
       $data = Properties::getMyProperties(0);
       return view('frontend.rentpoa',['data'=>$data]);
    }

    public function login()
    {
        return view('frontend.login');
    }

    public function doLogin(Request $request)
    {

        request()->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ], [
            'email.required' => 'Email is required',
        ]);

        $checkuser = User::where('email',request()->email)->first();
				if($checkuser) {

          if($checkuser->status == 0) {
            return redirect()->back()->withInput(Input::all())->with(['error'=>'Your account is not activated. Please visit check your email and click on the activation link.']);
          }
          else {

          $isBlocked = $checkuser->isBlocked;
          if($isBlocked == 0) {

        if (Auth::attempt(['email' => request()->email, 'password' => request()->password])) {
  				   User::where('email', request()->email)->update(['isOnline' => 1]);
				     return Redirect::to('dashboard');
  			} else {
  				   return redirect()->back()->withInput(Input::all())->with(['error'=>'Login failed. Wrong email or password']);
  			}

      }
      else {
          return redirect()->back()->withInput(Input::all())->with(['error'=>'This account is blocked. Please contact the site administrator at admin@rentpoa.com']);
      }
}
      }
      else {
          return redirect()->back()->withInput(Input::all())->with(['error'=>'Email address not found']);
      }

    }

    public function register()
    {
        return view('frontend.register');
    }

    public function doRegister(Request $request)
    {

        request()->validate([
            'firstName' => 'required|min:3|max:50',
            'lastName' => 'required|min:3|max:50',
            'mobileNo' => 'required|numeric|regex:/(07)[0-9]{8}/',// input starts with 01 and is followed by 9 numbers
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',//'confirm_password' => 'required|min:6|max:20|same:password',
            'terms' => 'accepted',
        ], [
            'firstName.required' => 'First Name is required',
            'firstName.min' => 'First Name must be at least 3 characters.',
            'firstName.max' => 'First Name should not be greater than 20 characters.',
            'lastName.required' => 'Last Name is required',
            'lastName.min' => 'Last Name must be at least 3 characters.',
            'lastName.max' => 'Last Name should not be greater than 20 characters.',
            'mobileNo.required' => 'Mobile Number is required',
            'mobileNo.regex' => 'Mobile number should be 10 digits starting with 07',
            'terms.required' => 'Please accept our terms of service',
        ]);

        // $input = request()->except('password','confirm_password');
        // $user=new User($input);
        // $user->password=bcrypt(request()->password);
        // $user->save();
        //request()->firstName,request()->lastName,request()->mobileNo,,request()->password
        $res = User::registerUser(request()->all());
        if($res) {
          return Redirect::to('login')->with('success', 'Successfully registration. Please check your email for an activation link');
        }
        else {
          return redirect()->back()->withInput(Input::all())->with('error', 'Registration failed. Please try again');
        }

    }

    public function addrental()
  	{
  		return view('frontend.addrental');
  	}

    public function termsOfService()
    {
        return view('frontend.termsOfService');
    }

  	/**
   * Create a new controller instance.
   *
   * @return void
   */

  public function postRental(Request $request)
  {

  		$propertySize = 0;
  		$bedrooms = 0;
  		$bathrooms = 0;
  		$parking = 0;
  		$hasBalconyy = 0;
  		$managementTypeId = 0;
  		$depositTypeId = 0;
  		$swimmingpool = 0;
  		$playground = 0;
  		$gated = 0;
  		$security = 0;
  		$cctv = 0;
  		$furnished = 0;
  		$negotiable = 0;
  		$backupgenerator = 0;
  		$liftelevator = 0;
  		$electricfence = 0;
  		$pets = 0;

  		request()->validate([
          'email' => 'required|email',
          'password' => 'required|min:6',
  				'uploadFile.*' => 'required|image|mimes:jpeg,png,jpg|max:2048',
  				'propertySectionId' => 'required|integer',
  				'categoryId' => 'required|integer',
  				'propertyTypeId' => 'required|integer',
  				'title' => 'required|min:3|max:100',
  				'description' => 'required|min:3|max:300',
  				'countyId' => 'required|integer',
  				'subCountyId' => 'required|integer',
  				'directions' => 'required|min:3|max:100',
          'amount' => 'required|integer',
  		]);

      $checkuser = User::where('email',request()->email)->first();
      if($checkuser) {

        if($checkuser->status == 0) {
          return redirect()->back()->withInput(Input::all())->with(['error'=>'Your account is not activated. Please visit check your email and click on the activation link.']);
        }
        else {

        $isBlocked = $checkuser->isBlocked;
        if($isBlocked == 0) {

      if (Auth::attempt(['email' => request()->email, 'password' => request()->password])) {

           User::where('email', request()->email)->update(['isOnline' => 1]);

  		$totalFiles = count($request->file('uploadFile')); //log::info("count photos--".$totalFiles);
  		if($totalFiles > 3) {

  			$subCountyId = request()->subCountyId;
  			$location = request()->location;
  			$title = request()->title;
  			$description = request()->description;
  			$directions = request()->directions;
        $categoryId = request()->categoryId;
  			$propertyTypeId = request()->propertyTypeId;
  			$propertySectionId = request()->propertySectionId;
        $amount = request()->amount;
  			// $propertySize = 0;
  			// $bedrooms = 0;
  			// $bathrooms = 0;
  			if (isset($request->parking) && $request->parking == 1) {
  				$parking = 1;
  			}
  			if (isset($request->hasBalcony) && $request->hasBalcony == 1) {
  				$hasBalconyy = 1;
  			}
  			if (isset($request->managementTypeId) && $request->managementTypeId == 1) {
  				$managementTypeId = 0;
  			}
  			// $depositTypeId = 0;
  			if (isset($request->swimmingpool) && $request->swimmingpool == 1) {
  				$swimmingpool = 1;
  			}
  			if (isset($request->playground) && $request->playground == 1) {
  				$playground = 1;
  			}
  			if (isset($request->gated) && $request->gated == 1) {
  				$gated = 1;
  			}
  			if (isset($request->security) && $request->security == 1) {
  				$security = 1;
  			}
  			if (isset($request->cctv) && $request->cctv == 1) {
  				$cctv = 1;
  			}
  			if (isset($request->furnished) && $request->furnished == 1) {
  				$furnished = 1;
  			}
  			if (isset($request->negotiable) && $request->negotiable == 1) {
  				$negotiable = 1;
  			}
  			if (isset($request->backupgenerator) && $request->backupgenerator == 1) {
  				$backupgenerator = 1;
  			}
  			if (isset($request->liftelevator) && $request->liftelevator == 1) {
  				$liftelevator = 1;
  			}
  			if (isset($request->electricfence) && $request->electricfence == 1) {
  				$electricfence = 1;
  			}
  			if (isset($request->pets) && $request->pets == 1) {
  				$pets = 1;
  			}

      $typeDetails = Propertytypes::where('id',$propertyTypeId)->first();

  		$post = Properties::saveProperty($categoryId,$amount,$subCountyId,$location,$title,$description,$directions,$propertyTypeId,$propertySectionId,$propertySize,$bedrooms,$bathrooms,$parking,$hasBalconyy,$managementTypeId,$depositTypeId,$swimmingpool,$playground,$gated,$security,$cctv,$furnished,$negotiable,$backupgenerator,$liftelevator,$electricfence,$pets);
  		if($post) {
  		foreach ($request->file('uploadFile') as $key => $value) {
  				$imageName = "RentPoa_Rent_Poa_Photo_".$typeDetails->typeName."_".time(). $key . '.' . $value->getClientOriginalExtension();
  				$value->move(public_path('allphotos'), $imageName);
          $img = Image::make(public_path('allphotos/'.$imageName.''));
          // crop the best fitting 1:1 ratio (200x200) and resize to 200x200 pixel
  				$img->fit(300);
          /* insert watermark at bottom-right corner with 10px offset */
          $img->insert(public_path('images/watermark.png'), 'bottom-right', 10, 10);
          $img->save(public_path('allphotos/'.$imageName.''));
  				Propertyphotos::savePhoto($post,$imageName);
  		}

      return Redirect::to('dashboard')->with('success', 'Your rental was successfully posted');

  	}
  	else {
  		return redirect()->back()->withInput(Input::all())->with('error', 'Failed to post your rental. Please try again');
  	}

  	}
  	else {
  		return redirect()->back()->withInput(Input::all())->with('error', 'Please upload minimum of 4 and maximum of 9 photos for this rental');
  	}
  } else {
       return redirect()->back()->withInput(Input::all())->with(['error'=>'Login failed. Wrong email or password']);
  }

}
else {
    return redirect()->back()->withInput(Input::all())->with(['error'=>'This account is blocked. Please contact the site administrator at admin@rentpoa.com']);
}
}
}
else {
    return redirect()->back()->withInput(Input::all())->with(['error'=>'Email address not found']);
}
  }

  public function submitPhone(Request $request) {
  if($request->ajax())
  {
  $mobileNo = $request->mobileNo;
  log::info("submitPhone  ---- mobileNo---".$mobileNo);

  return Customers::submitPhone($mobileNo);

  }
  else {
    return 500;
  }
}

public function verifyCode(Request $request) {
if($request->ajax())
{
$smsCode = $request->smsCode;
$mobileNo = $request->mobileNo;
log::info("verifyCode  ---- smsCode---".$smsCode);

$resc = Customers::verifyCode($smsCode,$mobileNo);
if($resc) {
return 200;
}
else {
return 500;
}
}
else {
return 500;
}
}

public function latestRentals()
{
   $data = Properties::getLatestProperties();
   return view('frontend.latestRentals',['data'=>$data]);
}

public function popularRentals()
{
   $data = Properties::getPopularProperties();
   return view('frontend.popularRentals',['data'=>$data]);
}

public function searchHome()
{
   $categoryId = Input::get('categoryId');
   $propertyTypeId = Input::get('propertyTypeId');
   $countyId = Input::get('countyId');
   $subCountyId = Input::get('subCountyId');
   $approximateCost = Input::get('approximateCost');
   $data = Properties::searchHomeProperties($categoryId,$propertyTypeId,$countyId,$subCountyId,$approximateCost);
   $count = count($data);
   return view('frontend.rentpoa',['data'=>$data,'count'=>$count]);
}

public function searchCategory()
{
   $categoryId = Input::get('categoryId');
   $propertyTypeId = Input::get('propertyTypeId');
   $countyId = Input::get('countyId');
   $subCountyId = Input::get('subCountyId');
   $approximateCost = Input::get('approximateCost');
   $categoryDetails = Propertycategories::where('id',$categoryId)->first();
   $data = Properties::searchHomeProperties($categoryId,$propertyTypeId,$countyId,$subCountyId,$approximateCost);
   $count = count($data);
   return view('frontend.viewbycategory',['data'=>$data,'count'=>$count,'categoryDetails'=>$categoryDetails]);
}

public function searchType()
{
   $propertyTypeId = Input::get('propertyTypeId');
   $countyId = Input::get('countyId');
   $subCountyId = Input::get('subCountyId');
   $approximateCost = Input::get('approximateCost');
   $typeDetails = Propertytypes::where('id',$propertyTypeId)->first();
   $data = Properties::searchTypeProperties($propertyTypeId,$countyId,$subCountyId,$approximateCost);
   $count = count($data);
   return view('frontend.viewbytype',['data'=>$data,'count'=>$count,'typeDetails'=>$typeDetails]);
}

public function searchLocation()
{
   $propertyTypeId = Input::get('propertyTypeId');
   $subCountyId = Input::get('subCountyId');
   $approximateCost = Input::get('approximateCost');
   $locationDetails = Subcounties::where('id',$subCountyId)->first();
   $data = Properties::searchLocationProperties($propertyTypeId,$subCountyId,$approximateCost);
   $count = count($data);
   return view('frontend.viewbylocation',['data'=>$data,'count'=>$count,'locationDetails'=>$locationDetails]);
}

  public function logout(Request $request) {
    Auth::logout();
    return redirect('/login');
  }


public function activateAccount(){

    $reset_code=input::get('c');
    $id=input::get('k');

    $m=User::where('id',$id)->first();

    if($m){

        $mt=User::where('id',$id)->first();
        Log::info('activate id--'.$id);
        Log::info('reset_code--'.$mt->reset_code);

        if($mt->status == 1){
        return Redirect::to('login')->with('success', 'Your account is already activated.Please login');
        }

      if($mt->reset_code==$reset_code){
        //make active

         $m=User::where('id',$id)->update(['status'=>1,'reset_code'=>0]);

        return Redirect::to('login')->with('success', 'You have successfully activated your account. Login.');

      }else{
         return Redirect::to('login')->with('error', 'Sorry, we could not verify your account. Contact admin for help.');
      }

    }else{
      return Redirect::to('login')->with('error', 'Invalid verification attempt');
    }
  }


}
