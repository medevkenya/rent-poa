<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use Hash;
use Session;
use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Properties;

class DashboardController extends Controller {

	public function Dashboard()
	{
		$userId = Auth::user()->id;
		$data = Properties::getMyProperties($userId);
		return view('frontend.dashboard',['data'=>$data]);
	}

	public function ChangePassword()
{
return view('frontend.changepassword');
}

public function dochangepassword(Request $request)
{

	request()->validate([
		'oldpassword' => 'required|min:6',
		'password' => 'min:6|different:oldpassword',
		'cpassword' => 'required|same:password',
], [
		'oldpassword.required' => 'Your current password is required',
		'oldpassword.min' => 'Current password must be at least 6 characters',
		'password.required' => 'New password is required',
		'password.min' => 'New password must be at least 6 characters',
		'password.different' => 'New password must be different from current password',
		'cpassword.required' => 'Please confirm your new password',
		'cpassword.same' => 'Password and confirm password must be the same',
]);

	$oldpassword	= request()->oldpassword;
  $password	= request()->password;
  				$cpassword 	= request()->cpassword;
          $userid	= Auth::user()->id;

					$oldhashedPassword = Hash::make($oldpassword);

					$checkold = User::where('id',$userid)->first();
          if (!Hash::check($oldpassword, $checkold->password)) {
						return back()->with('error', 'Your current password is wrong');
					}
 				else {

                 $hashedPassword = Hash::make($password);

                 $edituserpass = User::where('id', $userid)->update(['password' => $hashedPassword]);
                 if($edituserpass) {
									 return back()->with('success', 'Your password has been changed successfully');

   } else {
return back()->with('error', 'Failed to change your password');
   }
}
}


public function Profile()
{
	  $userid	= Auth::user()->id;
  $details = User::where('id',$userid)->first();

  return view('frontend.profile', ['details' => $details]);
}

public function updateProfile()
{
          $firstName	= Input::get('firstName');
  				$lastName 	= Input::get('lastName');

          $mobileNo 	= Input::get('mobileNo');
		  	$mobileNo = "254".substr($mobileNo, -9);

          $userid	= Auth::user()->id;

$edituser = User::where('id', $userid)->update(['firstName' => $firstName,'lastName' => $lastName,'mobileNo' => $mobileNo]);
if($edituser) {
return back()->with('success', 'Your profile has been updated successfully');
   } else {
return back()->with('error', 'Failed to update your profile');

   }
}

public function mypic()
{
return view('frontend.mypic');
}
public function editPic() {

  // getting all of the post data
  $id	= Auth::user()->id;
  $file = array('image' => Input::file('profilepic'));

  // setting up rules
//  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000 required|image|mimes:jpeg,png,jpg,gif,svg|max:2048
  $rules = array('image' => 'required|image|mimes:jpeg,png,jpg|max:5048',);
  // doing the validation, passing post data, rules and the messages
  $validator = Validator::make($file, $rules);

        // checking file is valid.
    if (Input::file('profilepic')->isValid()) {
      //$destinationPath = 'otheruploads'; // upload path
      $destinationPath = public_path('/profiles');

      $extension = Input::file('profilepic')->getClientOriginalExtension(); // getting image extension

      $fileName = rand(11111,99999).'.'.$extension; // renameing image

      Input::file('profilepic')->move($destinationPath, $fileName); // uploading file to given path

      //file name to take to db
      $profilepic=  $fileName;

$editlogo = User::where('id', $id)->update(['profilepic' => $profilepic]);
return back()->with('success', 'Your profile picture was updated successfully');

    }
    else {
return back()->with('error', 'Failed to update your picture');

  }
}

		public function ErrorPage()
	{
		LogsController::CreateSystemLog('Received ErrorPage');
		$datee = date('Y-m-d');
    return view('frontend.error');
	}

}
