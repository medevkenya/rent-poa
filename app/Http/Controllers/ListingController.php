<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Properties;
use App\Propertytypes;
use App\Subcounties;
use App\Propertyphotos;
use Intervention\Image\Facades\Image as Image;

class ListingController extends Controller {

	public function addListing()
	{
		return view('frontend.listing');
	}

	/**
 * Create a new controller instance.
 *
 * @return void
 */

public function postListing(Request $request)
{

		$propertySize = 0;
		$bedrooms = 0;
		$bathrooms = 0;
		$parking = 0;
		$hasBalconyy = 0;
		$managementTypeId = 0;
		$depositTypeId = 0;
		$swimmingpool = 0;
		$playground = 0;
		$gated = 0;
		$security = 0;
		$cctv = 0;
		$furnished = 0;
		$negotiable = 0;
		$backupgenerator = 0;
		$liftelevator = 0;
		$electricfence = 0;
		$pets = 0;

		request()->validate([
				'uploadFile.*' => 'required|image|mimes:jpeg,png,jpg|max:2048',
				'propertySectionId' => 'required|integer',
				'categoryId' => 'required|integer',
				'propertyTypeId' => 'required|integer',
				'title' => 'required|min:3|max:100',
				'description' => 'required|min:3|max:300',
				'countyId' => 'required|integer',
				'subCountyId' => 'required|integer',
				'directions' => 'required|min:3|max:100',
				'amount' => 'required|integer',
		]);

		$totalFiles = count($request->file('uploadFile')); log::info("count photos--".$totalFiles);
		if($totalFiles > 3) {

			$subCountyId = request()->subCountyId;
			$location = request()->location;
			$title = request()->title;
			$description = request()->description;
			$directions = request()->directions;
			$categoryId = request()->categoryId;
			$propertyTypeId = request()->propertyTypeId;
			$propertySectionId = request()->propertySectionId;
			$amount = request()->amount;
			// $propertySize = 0;
			// $bedrooms = 0;
			// $bathrooms = 0;
			if (isset($request->parking) && $request->parking == 1) {
				$parking = 1;
			}
			if (isset($request->hasBalcony) && $request->hasBalcony == 1) {
				$hasBalconyy = 1;
			}
			if (isset($request->managementTypeId) && $request->managementTypeId == 1) {
				$managementTypeId = 0;
			}
			// $depositTypeId = 0;
			if (isset($request->swimmingpool) && $request->swimmingpool == 1) {
				$swimmingpool = 1;
			}
			if (isset($request->playground) && $request->playground == 1) {
				$playground = 1;
			}
			if (isset($request->gated) && $request->gated == 1) {
				$gated = 1;
			}
			if (isset($request->security) && $request->security == 1) {
				$security = 1;
			}
			if (isset($request->cctv) && $request->cctv == 1) {
				$cctv = 1;
			}
			if (isset($request->furnished) && $request->furnished == 1) {
				$furnished = 1;
			}
			if (isset($request->negotiable) && $request->negotiable == 1) {
				$negotiable = 1;
			}
			if (isset($request->backupgenerator) && $request->backupgenerator == 1) {
				$backupgenerator = 1;
			}
			if (isset($request->liftelevator) && $request->liftelevator == 1) {
				$liftelevator = 1;
			}
			if (isset($request->electricfence) && $request->electricfence == 1) {
				$electricfence = 1;
			}
			if (isset($request->pets) && $request->pets == 1) {
				$pets = 1;
			}

			$typeDetails = Propertytypes::where('id',$propertyTypeId)->first();

		$post = Properties::saveProperty($categoryId,$amount,$subCountyId,$location,$title,$description,$directions,$propertyTypeId,$propertySectionId,$propertySize,$bedrooms,$bathrooms,$parking,$hasBalconyy,$managementTypeId,$depositTypeId,$swimmingpool,$playground,$gated,$security,$cctv,$furnished,$negotiable,$backupgenerator,$liftelevator,$electricfence,$pets);
		if($post) {
		foreach ($request->file('uploadFile') as $key => $value) {
				$imageName = "RentPoa_Rent_Poa_Photo_".$typeDetails->typeName."_".time(). $key . '.' . $value->getClientOriginalExtension();
				$value->move(public_path('allphotos'), $imageName);
				//$value->move(public_path('allphotos'), $imageName);
				$img = Image::make(public_path('allphotos/'.$imageName.''));
				// crop the best fitting 1:1 ratio (200x200) and resize to 200x200 pixel
				$img->fit(300);
				/* insert watermark at bottom-right corner with 10px offset */
				$img->insert(public_path('images/watermark.png'), 'bottom-right', 10, 10);
				$img->save(public_path('allphotos/'.$imageName.''));
				Propertyphotos::savePhoto($post,$imageName);
		}

		return back()->with('success', 'Your rental was successfully posted');
	}
	else {
		return back()->with('error', 'Failed to post your rental. Please try again');
	}

	}
	else {
		return back()->with('error', 'Please upload minimum of 4 and maximum of 9 photos for this rental');
	}
}


public function doEditRental(Request $request)
{

		$propertySize = 0;
		$bedrooms = 0;
		$bathrooms = 0;
		$parking = 0;
		$hasBalconyy = 0;
		$managementTypeId = 0;
		$depositTypeId = 0;
		$swimmingpool = 0;
		$playground = 0;
		$gated = 0;
		$security = 0;
		$cctv = 0;
		$furnished = 0;
		$negotiable = 0;
		$backupgenerator = 0;
		$liftelevator = 0;
		$electricfence = 0;
		$pets = 0;

		request()->validate([
			'id' => 'required|integer',
				'propertySectionId' => 'required|integer',
				'categoryId' => 'required|integer',
				'propertyTypeId' => 'required|integer',
				'title' => 'required|min:3|max:100',
				'description' => 'required|min:3|max:300',
				'countyId' => 'required|integer',
				'subCountyId' => 'required|integer',
				'directions' => 'required|min:3|max:100',
				'amount' => 'required|integer',
		]);

		  $id = request()->id;
			$subCountyId = request()->subCountyId;
			$location = request()->location;
			$title = request()->title;
			$description = request()->description;
			$directions = request()->directions;
			$propertyTypeId = request()->propertyTypeId;
			$propertySectionId = request()->propertySectionId;
			$amount = request()->amount;
			// $propertySize = 0;
			// $bedrooms = 0;
			// $bathrooms = 0;
			if (isset($request->parking) && $request->parking == 1) {
				$parking = 1;
			}
			if (isset($request->hasBalcony) && $request->hasBalcony == 1) {
				$hasBalconyy = 1;
			}
			if (isset($request->managementTypeId) && $request->managementTypeId == 1) {
				$managementTypeId = 0;
			}
			// $depositTypeId = 0;
			if (isset($request->swimmingpool) && $request->swimmingpool == 1) {
				$swimmingpool = 1;
			}
			if (isset($request->playground) && $request->playground == 1) {
				$playground = 1;
			}
			if (isset($request->gated) && $request->gated == 1) {
				$gated = 1;
			}
			if (isset($request->security) && $request->security == 1) {
				$security = 1;
			}
			if (isset($request->cctv) && $request->cctv == 1) {
				$cctv = 1;
			}
			if (isset($request->furnished) && $request->furnished == 1) {
				$furnished = 1;
			}
			if (isset($request->negotiable) && $request->negotiable == 1) {
				$negotiable = 1;
			}
			if (isset($request->backupgenerator) && $request->backupgenerator == 1) {
				$backupgenerator = 1;
			}
			if (isset($request->liftelevator) && $request->liftelevator == 1) {
				$liftelevator = 1;
			}
			if (isset($request->electricfence) && $request->electricfence == 1) {
				$electricfence = 1;
			}
			if (isset($request->pets) && $request->pets == 1) {
				$pets = 1;
			}

		$post = Properties::editProperty($id,$amount,$subCountyId,$location,$title,$description,$directions,$propertyTypeId,$propertySectionId,$propertySize,$bedrooms,$bathrooms,$parking,$hasBalconyy,$managementTypeId,$depositTypeId,$swimmingpool,$playground,$gated,$security,$cctv,$furnished,$negotiable,$backupgenerator,$liftelevator,$electricfence,$pets);
		if($post) {
		return back()->with('success', 'Your rental was successfully updated');
	}
	else {
		return back()->with('error', 'Failed to update your rental. Please try again');
	}

}

public function viewRental($propertyId)
{
	$userId	= Auth::user()->id;
	$check = Properties::where('id',$propertyId)->where('userId',$userId)->where('is_deleted',0)->first();
	if($check) {
		$propertyDetails = Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','propertysection.id as propertysectionId','propertysection.name as propertysectionName','subcounties.id as subcountyId','subcounties.subCountyName')
		->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
		->leftJoin('propertysection','properties.propertysectionId','=','propertysection.id')
		->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
		->where('properties.id',$propertyId)
		->first();
return view('frontend.viewRental',['propertyDetails'=>$propertyDetails]);
}
else {
	return back()->with('error', 'Property does not exist');
}
}

public function editRental($propertyId)
{
	$userId	= Auth::user()->id;
	$check = Properties::where('id',$propertyId)->where('userId',$userId)->where('is_deleted',0)->first();
	if($check) {
		$propertyDetails = Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','propertysection.id as propertysectionId','propertysection.name as propertysectionName','subcounties.id as subcountyId','subcounties.subCountyName')
		->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
		->leftJoin('propertysection','properties.propertysectionId','=','propertysection.id')
		->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
		->where('properties.id',$propertyId)
		->first();
return view('frontend.editRental',['propertyDetails'=>$propertyDetails]);
}
else {
	return back()->with('error', 'Property does not exist');
}
}

public function addPhoto() {

  // getting all of the post data
	$propertyId = Input::get('propertyId');

	$checCount = Propertyphotos::where('propertyId',$propertyId)->where('is_deleted',0)->count();
	if($checCount < 9) {

  $id	= Auth::user()->id;
  $file = array('image' => Input::file('photo'));

  // setting up rules
//  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000 required|image|mimes:jpeg,png,jpg,gif,svg|max:2048
  $rules = array('image' => 'required|image|mimes:jpeg,png,jpg|max:5048',);
  // doing the validation, passing post data, rules and the messages
  $validator = Validator::make($file, $rules);

        // checking file is valid.
    if (Input::file('photo')->isValid()) {
      //$destinationPath = 'otheruploads'; // upload path
      $destinationPath = public_path('/allphotos');

      $extension = Input::file('photo')->getClientOriginalExtension(); // getting image extension

      $imageName = time().'.'.$extension; // renameing image

      Input::file('photo')->move($destinationPath, $imageName); // uploading file to given path

		$save =	Propertyphotos::savePhoto($propertyId,$imageName);
		if($save) {
return back()->with('success', 'New photo was uploaded successfully');
}
else {
	return back()->with('error', 'Failed to upload photo. Please try again');
}
    }
    else {
return back()->with('error', 'Failed to upload photo');
  }
}
else {
	return back()->with('error', 'This rental already has 9 (Maximum) photos');
}
}

public function removePhoto($id)
{
	$userId	= Auth::user()->id;
	$check = Propertyphotos::removePhoto($id);
	if($check) {
	return back()->with('success', 'Photo removed successfully');
}
else {
	return back()->with('error', 'Failed to remove photo. Please try again');
}
}

public function removeRental($id)
{
	$userId	= Auth::user()->id;
	$check = Properties::removeRental($id);
	if($check) {
	return back()->with('success', 'Rental was removed successfully');
}
else {
	return back()->with('error', 'Failed to remove rental. Please try again');
}
}

	public function getRentalTypes(Request $request)
	{

    if (!$request->categoryId) {
        $html = '<option value="">Select Category First</option>';
    } else {
        $html = '';
        $data = Propertytypes::where('categoryId',$request->categoryId)->where('is_deleted',0)->get();
        //$html = '<option value="">Select Type</option>';
        foreach ($data as $dt) {

            $html .= '<option value="'.$dt->id.'">'.$dt->typeName.'</option>';
        }
    }

    return response()->json(['html' => $html]);
	}

	public function getSubcounties(Request $request)
	{
		if (!$request->countyId) {
				$html = '<option value="">Select County First</option>';
		} else {
				$html = '';
				$data = Subcounties::where('countyId',$request->countyId)->where('is_deleted',0)->get();
				//$html = '<option value="">Select Location</option>';
				foreach ($data as $dt) {

						$html .= '<option value="'.$dt->id.'">'.$dt->subCountyName.'</option>';
				}
		}

		return response()->json(['html' => $html]);
	}

}
