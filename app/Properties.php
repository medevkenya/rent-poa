<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class Properties extends Model
{

    protected $table = 'properties';

    public static function getMyProperties($userId) { //,'deposittypes.id as deposittypeId','deposittypes.name as deposittypeName'
      $data = array();
      if($userId == 0) {
      $arr = Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','users.firstName','users.lastName','users.profilepic','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('users','properties.userId','=','users.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)
      ->orderBy('properties.id','DESC')->paginate(40);
    }
    else {
      $arr = Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','propertysection.id as propertysectionId','propertysection.name as propertysectionName','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('propertysection','properties.propertysectionId','=','propertysection.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.userId',$userId)
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)->orderBy('properties.id','DESC')->paginate(40);
    }
      return $arr;
    }

    public static function saveProperty($categoryId,$amount,$subCountyId,$location,$title,$description,$directions,$propertyTypeId,$propertySectionId,$propertySize,$bedrooms,$bathrooms,$parking,$hasBalcony,$managementTypeId,$depositTypeId,$swimmingpool,$playground,$gated,$security,$cctv,$furnished,$negotiable,$backupgenerator,$liftelevator,$electricfence,$pets) {

      $pid = strtoupper(date('d').str_random(3).date('m'));

      $model = new Properties;
      $model->pid = $pid;
      $model->userId = Auth::user()->id;
      $model->subCountyId = $subCountyId;
      $model->location = $location;
      $model->title = $title;
      $model->description = $description;
      $model->directions = $directions;
      $model->categoryId = $categoryId;
      $model->propertyTypeId = $propertyTypeId;
      $model->propertySectionId = $propertySectionId;
      $model->propertySize = $propertySize;
      $model->bedrooms = $bedrooms;
      $model->bathrooms = $bathrooms;
      $model->parking = $parking;
      $model->hasBalcony = $hasBalcony;
      $model->managementTypeId = $managementTypeId;
      $model->depositTypeId = $depositTypeId;
      $model->swimmingpool = $swimmingpool;
      $model->playground = $playground;
      $model->gated = $gated;
      $model->security = $security;
      $model->cctv = $cctv;
      $model->furnished = $furnished;
      $model->negotiable = $negotiable;
      $model->backupgenerator = $backupgenerator;
      $model->liftelevator = $liftelevator;
      $model->electricfence = $electricfence;
      $model->pets = $pets;
      $model->amount = $amount;
      $model->save();
      if($model) {
        return $model->id;
      }
      else {
        return false;
      }
    }

    public static function editProperty($id,$amount,$subCountyId,$location,$title,$description,$directions,$propertyTypeId,$propertySectionId,$propertySize,$bedrooms,$bathrooms,$parking,$hasBalcony,$managementTypeId,$depositTypeId,$swimmingpool,$playground,$gated,$security,$cctv,$furnished,$negotiable,$backupgenerator,$liftelevator,$electricfence,$pets) {

      $model = Properties::find($id);
      $model->subCountyId = $subCountyId;
      $model->location = $location;
      $model->title = $title;
      $model->description = $description;
      $model->directions = $directions;
      $model->propertyTypeId = $propertyTypeId;
      $model->propertySectionId = $propertySectionId;
      $model->propertySize = $propertySize;
      $model->bedrooms = $bedrooms;
      $model->bathrooms = $bathrooms;
      $model->parking = $parking;
      $model->hasBalcony = $hasBalcony;
      $model->managementTypeId = $managementTypeId;
      $model->depositTypeId = $depositTypeId;
      $model->swimmingpool = $swimmingpool;
      $model->playground = $playground;
      $model->gated = $gated;
      $model->security = $security;
      $model->cctv = $cctv;
      $model->furnished = $furnished;
      $model->negotiable = $negotiable;
      $model->backupgenerator = $backupgenerator;
      $model->liftelevator = $liftelevator;
      $model->electricfence = $electricfence;
      $model->pets = $pets;
      $model->amount = $amount;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

    public static function removeRental($id) {
        $model = Properties::find($id);
        $model->is_deleted = 1;
        $model->save();
        if ($model) {
          return true;
        }
        else {
          return false;
        }
    }

    public static function getPropertiesByLocation($subCountyId) {
      $arr = Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','users.firstName','users.lastName','users.profilepic','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('users','properties.userId','=','users.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.subCountyId',$subCountyId)
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)
      ->orderBy('properties.id','DESC')->paginate(40);
      return $arr;
    }

    public static function getPropertiesByCategory($categoryId) {

      $arr = Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','users.firstName','users.lastName','users.profilepic','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('users','properties.userId','=','users.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.categoryId',$categoryId)
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)
      ->orderBy('properties.id','DESC')->paginate(40);

      return $arr;
    }

    public static function getPropertiesByType($typeId) {

      $arr = Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','users.firstName','users.lastName','users.profilepic','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('users','properties.userId','=','users.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.propertyTypeId',$typeId)
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)
      ->orderBy('properties.id','DESC')->paginate(40);

      return $arr;
    }

    public static function getPropertyById($propertyId) {
      return Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','users.firstName','users.lastName','users.mobileNo','users.isVerified','users.profilepic','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('users','properties.userId','=','users.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.id',$propertyId)
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)->first();
    }

    public static function getLatestProperties() {
      return Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','users.firstName','users.lastName','users.mobileNo','users.isVerified','users.profilepic','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('users','properties.userId','=','users.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)->orderBy('properties.id','DESC')->paginate(40);
    }

    public static function getPopularProperties() {
      return Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','users.firstName','users.lastName','users.mobileNo','users.isVerified','users.profilepic','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('users','properties.userId','=','users.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)->orderBy('views','DESC')->paginate(40);
    }

    public static function searchHomeProperties($categoryId,$propertyTypeId,$countyId,$subCountyId,$approximateCost) {
      $min_price = explode("-", $approximateCost)[0];
      $max_price = substr($approximateCost, strpos($approximateCost, "-") + 1);

      $arr = Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','users.firstName','users.lastName','users.profilepic','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('users','properties.userId','=','users.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.categoryId',$categoryId)
      ->where('properties.propertyTypeId',$propertyTypeId)
      ->where('properties.subCountyId',$subCountyId)
      ->whereBetween('properties.amount', [$min_price, $max_price])
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)
      ->orderBy('properties.id','DESC')->paginate(40);
      return $arr;
    }

    public static function searchTypeProperties($propertyTypeId,$countyId,$subCountyId,$approximateCost) {
      $min_price = explode("-", $approximateCost)[0];
      $max_price = substr($approximateCost, strpos($approximateCost, "-") + 1);

      $arr = Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','users.firstName','users.lastName','users.profilepic','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('users','properties.userId','=','users.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.propertyTypeId',$propertyTypeId)
      ->where('properties.subCountyId',$subCountyId)
      ->whereBetween('properties.amount', [$min_price, $max_price])
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)
      ->orderBy('properties.id','DESC')->paginate(40);
      return $arr;
    }

    public static function searchLocationProperties($propertyTypeId,$subCountyId,$approximateCost) {
      $min_price = explode("-", $approximateCost)[0];
      $max_price = substr($approximateCost, strpos($approximateCost, "-") + 1);

      $arr = Properties::select('properties.*','propertytypes.id as propertytypeId','propertytypes.typeName as propertytypeName','users.firstName','users.lastName','users.profilepic','subcounties.id as subcountyId','subcounties.subCountyName')
      ->leftJoin('propertytypes','properties.propertyTypeId','=','propertytypes.id')
      ->leftJoin('users','properties.userId','=','users.id')
      ->leftJoin('subcounties','properties.subCountyId','=','subcounties.id')
      ->where('properties.propertyTypeId',$propertyTypeId)
      ->where('properties.subCountyId',$subCountyId)
      ->whereBetween('properties.amount', [$min_price, $max_price])
      ->where('properties.status',1)
      ->where('properties.is_deleted',0)
      ->orderBy('properties.id','DESC')->paginate(40);
      return $arr;
    }

    public static function updateViews($propertyId) {
      $model = Properties::find($propertyId);
      $model->views = $model->views + 1;
      $model->save();
    }

}
