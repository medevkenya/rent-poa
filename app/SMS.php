<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use AfricasTalking\SDK\AfricasTalking;

class SMS extends Model
{

    protected $table = 'sms';

    public static function SendSMS($mobileNo,$message) {
      // Specify your login credentials
      $username   = "Farm-Up";
      $apiKey     = "a646220c8d95cd5eea47aa577c5de330b8ace70bbf3e9e16bff7091cc0b42925";
      $senderid = "Farm-Up";

      $AT       = new AfricasTalking($username, $apiKey);

      // Get one of the services
      $sms      = $AT->sms();

      // Use the service
      $result   = $sms->send([
        'from'      => $senderid,
        'to'        => $mobileNo,
        'message'   => $message
      ]);

      return true;
      //print_r($result);
    }

}
