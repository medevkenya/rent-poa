<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class Subcounties extends Model
{

    protected $table = 'subcounties';

    public static function getByCountyId($countyId) {
      return Subcounties::where('countyId',$countyId)->where('is_deleted',0)->get();
    }

    public static function getAll() {
      return Subcounties::leftJoin('properties','subcounties.id','=','properties.subCountyId')
           ->selectRaw('subcounties.*, count(properties.subCountyId) AS `count`')
           ->where('subcounties.is_deleted',0)
           ->groupBy('subcounties.id')
           ->orderBy('count','DESC')
           ->limit(30)
           ->get();
    }

}
