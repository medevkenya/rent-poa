<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mail;
use Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','firstName','lastName','mobileNo', 'email', 'password','profilepic',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getUserById($id) {
      return User::select('id','email','mobileNo','notifyMe','firstName','lastName','isOnline','isBlocked')
      ->where('id',$id)->where('is_deleted',0)->first();
    }

    public static function registerUser($all) {
      $model = new User;
      $model->firstName = $all['firstName'];
      $model->lastName = $all['lastName'];
      $model->mobileNo = "254".substr($all['mobileNo'], -9);
      $model->email = $all['email'];
      $model->password = Hash::make($all['password']);
      $model->save();
      if($model) {

        $email = $all['email'];

        $id = $model->id;
        $code=md5(date('Y-m-d: H:i:sa').$id);
        User::where(['id'=>$id])->update(['reset_code'=>$code]);

         $link=url('/activateAccount?&c='.$code.'&k='.$id);

         $datanot=array('name'=>$all['firstName']." ".$all['lastName'],'email'=>$all['email'],'link'=>$link);

         Mail::send('emails.regtemp', $datanot, function($message) use ($email) {
         $message->to($email)->subject('Activate Account');
         $message->from('mails@rentpoa.com','Rent Poa');
         });

        return true;

      }
      else {
        return false;
      }
    }

    public static function resetPassword($email) {

    $userdetails = User::where('email',$email)->first();
    $pass=str_random(32);
    $name = $userdetails->firstName." ".$userdetails->lastName;

    $data=array('pass'=>$pass,'name'=>$name,'email'=>$email,);

    $edituserdata = User::where('email', $email)->update(['confirmpassword' => $pass]);
    if ($edituserdata) {

      Mail::send('emails.forgotpintemplate', $data, function($message) use ($pass,$email) {
      $message->to($email)->subject('Recover Password');
      $message->from('mails@rentpoa.com','Rent Poa');
      });

    return true;

    }
    else {
      return false;
    }

    }

}
