<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;

class Emaillogs extends Model
{
    protected $table = 'emaillogs';

    public static function saveMail($userId,$subject,$message,$priority,$template) {
      $model = new Emaillogs;
      $model->userId = $userId;
      $model->subject = $subject;
      $model->message = $message;
      $model->priority = $priority;
      $model->template = $template;
      $model->save();

      if($priority == 1) {
          self::sendNow($model->id);
      }

    }

    public static function sendNow($id) {

      $check = Emaillogs::where('id', $id)->first();
      $template = $check->template;
      $subject = $check->subject;
      $mainMessage = $check->message;
      
      $userDetails = User::getUserById($check->userId);
      $email = $userDetails->email;
      $firstName = $userDetails->firstName;
      $lastName = $userDetails->lastName;

      $datanot=array('name'=>$firstName." ".$lastName,'email'=>$email,'subject'=>$subject,'mainMessage'=>$mainMessage);

      $sent = Mail::send('emails.'.$template.'', $datanot, function($message) use ($email,$subject) {
      $message->to($email)->subject($subject);
      $message->from('mails@rentpoa.com','Rent Poa');
      });
      if($sent) {
        Emaillogs::where(['id'=>$id])->update(['sent'=>1]);
      }
      else {
        Emaillogs::where(['id'=>$id])->update(['sent'=>2]);
      }

    }

}
