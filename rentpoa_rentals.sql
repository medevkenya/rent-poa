-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 11, 2020 at 09:51 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentpoa_rentals`
--

-- --------------------------------------------------------

--
-- Table structure for table `counties`
--

CREATE TABLE `counties` (
  `id` int(11) NOT NULL,
  `countyName` varchar(50) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `counties`
--

INSERT INTO `counties` (`id`, `countyName`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Nairobi', 0, '2020-02-07 13:41:39', '2020-02-07 13:41:39'),
(2, 'Machakos', 0, '2020-02-07 13:41:39', '2020-02-07 13:41:39');

-- --------------------------------------------------------

--
-- Table structure for table `customerpropertycontacts`
--

CREATE TABLE `customerpropertycontacts` (
  `id` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `propertyId` int(11) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `mobileNo` varchar(20) NOT NULL,
  `verified` tinyint(3) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `deposittypes`
--

CREATE TABLE `deposittypes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `months` int(2) NOT NULL,
  `amount` int(6) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `deposittypes`
--

INSERT INTO `deposittypes` (`id`, `name`, `months`, `amount`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'No Deposit', 0, 0, 0, '2020-02-17 13:50:55', '2020-02-17 13:50:55'),
(2, '1 Per Month', 1, 20000, 0, '2020-02-17 13:50:55', '2020-02-17 13:50:55');

-- --------------------------------------------------------

--
-- Table structure for table `emaillogs`
--

CREATE TABLE `emaillogs` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `template` varchar(20) NOT NULL,
  `priority` int(3) NOT NULL DEFAULT '0',
  `sent` tinyint(3) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `amount` int(5) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`id`, `name`, `amount`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Security Fee', 500, 0, '2020-02-19 07:40:17', '2020-02-19 07:40:17'),
(2, 'Garbage Fee', 300, 0, '2020-02-19 07:40:17', '2020-02-19 07:40:17'),
(3, 'Viewing Fee', 1000, 0, '2020-02-19 07:40:36', '2020-02-19 07:40:36');

-- --------------------------------------------------------

--
-- Table structure for table `managementtypes`
--

CREATE TABLE `managementtypes` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `managementtypes`
--

INSERT INTO `managementtypes` (`id`, `name`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Landlord', 0, '2020-02-17 13:49:45', '2020-02-17 13:49:45'),
(2, 'Agent/Office', 0, '2020-02-17 13:49:45', '2020-02-17 13:49:45'),
(3, 'Caretaker', 0, '2020-02-17 13:49:53', '2020-02-17 13:49:53');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `pid` varchar(12) NOT NULL,
  `userId` int(11) NOT NULL,
  `subCountyId` int(11) NOT NULL,
  `location` varchar(50) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `directions` varchar(100) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `propertyTypeId` int(5) NOT NULL,
  `amount` double DEFAULT NULL,
  `propertySectionId` int(5) NOT NULL,
  `propertySize` varchar(20) NOT NULL,
  `bedrooms` int(5) NOT NULL,
  `bathrooms` int(5) NOT NULL,
  `parking` int(5) NOT NULL,
  `floorNo` int(2) DEFAULT NULL,
  `hasBalcony` tinyint(3) NOT NULL DEFAULT '0',
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `managementTypeId` int(5) NOT NULL,
  `depositTypeId` int(5) NOT NULL,
  `swimmingpool` tinyint(3) NOT NULL DEFAULT '0',
  `playground` tinyint(3) NOT NULL DEFAULT '0',
  `gated` tinyint(3) NOT NULL DEFAULT '0',
  `security` tinyint(3) NOT NULL DEFAULT '0',
  `cctv` tinyint(3) NOT NULL DEFAULT '0',
  `furnished` tinyint(3) NOT NULL DEFAULT '0',
  `negotiable` tinyint(3) NOT NULL DEFAULT '0',
  `backupgenerator` tinyint(3) NOT NULL DEFAULT '0',
  `liftelevator` tinyint(3) NOT NULL DEFAULT '0',
  `electricfence` tinyint(3) NOT NULL DEFAULT '0',
  `pets` tinyint(3) NOT NULL DEFAULT '0',
  `description` varchar(500) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `pid`, `userId`, `subCountyId`, `location`, `title`, `directions`, `categoryId`, `propertyTypeId`, `amount`, `propertySectionId`, `propertySize`, `bedrooms`, `bathrooms`, `parking`, `floorNo`, `hasBalcony`, `latitude`, `longitude`, `managementTypeId`, `depositTypeId`, `swimmingpool`, `playground`, `gated`, `security`, `cctv`, `furnished`, `negotiable`, `backupgenerator`, `liftelevator`, `electricfence`, `pets`, `description`, `views`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, '14A1', 3, 1, 'Embakasi Nairobi Kenya', 'Big Villa House', 'On your way to Kikutu, along mombasa bus terminus', 1, 1, 17500, 1, '5', 2, 3, 2, 3, 1, NULL, NULL, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, NULL, 0, 1, 0, '2020-02-21 11:01:50', '2020-02-21 11:02:26'),
(2, '14A1', 3, 1, 'Embakasi Nairobi Kenya', 'Spacious apartment in Kangundo', 'On your way to Kikutu, along mombasa bus terminus', 1, 1, 17500, 1, '5', 2, 3, 2, 3, 1, NULL, NULL, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, NULL, 0, 1, 0, '2020-02-21 11:01:50', '2020-02-21 11:02:26'),
(3, '14A1', 3, 1, 'Cool breez bedsitter for rent', 'Big Villa House', 'On your way to Kikutu, along mombasa bus terminus', 1, 1, 17500, 1, '5', 2, 3, 2, 3, 1, NULL, NULL, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, NULL, 0, 1, 0, '2020-02-21 11:01:50', '2020-02-21 11:02:26'),
(4, '14A1', 3, 1, 'Embakasi Nairobi Kenya', 'Ample parking for you', 'On your way to Kikutu, along mombasa bus terminus', 1, 1, 17500, 1, '5', 2, 3, 2, 3, 1, NULL, NULL, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, NULL, 0, 1, 0, '2020-02-21 11:01:50', '2020-02-21 11:02:26'),
(5, '14A1', 1, 1, NULL, 'dddddddd', 'nnnnnnnnnnnnnnnn', 1, 2, 17500, 1, '0', 0, 0, 0, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'ggggg', 1, 1, 0, '2020-02-29 02:55:19', '2020-03-07 02:35:19'),
(6, '14A1', 1, 1, NULL, 'dddddddd', 'nnnnnnnnnnnnnnnn', 1, 2, 17500, 1, '0', 0, 0, 0, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'ggggg', 0, 1, 0, '2020-02-29 02:55:19', '2020-02-29 02:55:19'),
(7, '14A1', 1, 1, NULL, 'dddddddd', 'nnnnnnnnnnnnnnnn', 1, 2, 17500, 1, '0', 0, 0, 0, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'ggggg', 1, 1, 0, '2020-02-29 02:55:19', '2020-03-07 12:38:02'),
(8, '14A1', 1, 1, NULL, 'dddddddd', 'nnnnnnnnnnnnnnnn', 1, 2, 17500, 1, '0', 0, 0, 0, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'ggggg', 0, 1, 0, '2020-02-29 02:55:19', '2020-02-29 02:55:19'),
(9, '14A1', 1, 1, NULL, 'ooooooooooooo', 'llllllllllllllllllll 2020', 1, 6, 17500, 1, '0', 0, 0, 1, NULL, 1, NULL, NULL, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 'pppppppppppp', 1, 1, 0, '2020-02-29 03:06:31', '2020-03-07 02:22:32'),
(10, '29LLz02', 3, 9, NULL, 'vvvvvvvvvvvvv', 'dddddddddd', 1, 2, 17500, 1, '0', 0, 0, 1, NULL, 1, NULL, NULL, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 'nnnnnnnnnn', 2, 1, 0, '2020-02-29 18:42:52', '2020-03-07 12:38:21'),
(11, '07S3803', 3, 1, NULL, 'bbbMMMM99', 'kkkkk', 1, 1, NULL, 1, '0', 0, 0, 0, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'bbbboo', 0, 1, 0, '2020-03-07 15:04:33', '2020-03-11 06:15:43'),
(12, '105NW03', 3, 2, NULL, 'Cheap besitters', 'fika hapo', 1, 2, 9500, 1, '0', 0, 0, 0, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'kuja home', 1, 1, 0, '2020-03-10 10:16:36', '2020-03-11 06:15:39'),
(13, '11UC403', 9, 3, NULL, 'Single Rooms Available at Junction Mall', 'Near the road', 1, 5, 5497, 1, '0', 0, 0, 0, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Free for occupation from today', 0, 1, 0, '2020-03-11 06:10:52', '2020-03-11 06:15:36'),
(14, '11IKH03', 9, 7, NULL, 'Vacant Bedsitter for rent near kitengela Kobil Pet', 'Near Pt', 1, 2, 7200, 1, '0', 0, 0, 0, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Vacant Bedsitter for rent near kitengela KOBIL Petrol station', 1, 1, 0, '2020-03-11 06:20:17', '2020-03-11 06:26:14');

-- --------------------------------------------------------

--
-- Table structure for table `propertycategories`
--

CREATE TABLE `propertycategories` (
  `id` int(11) NOT NULL,
  `categoryName` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `propertycategories`
--

INSERT INTO `propertycategories` (`id`, `categoryName`, `icon`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Rental Houses', 'fa fa-laptop icon-bg-1', 0, '2020-02-17 09:47:05', '2020-02-29 22:33:33'),
(2, 'Office Space', 'fa fa-apple icon-bg-2', 0, '2020-02-17 09:47:05', '2020-02-29 22:33:24'),
(3, 'Shops to Let', 'fa fa-home icon-bg-3', 0, '2020-02-17 09:47:38', '2020-02-29 22:33:46'),
(4, 'Venues & Furnitures', 'fa fa-shopping-basket icon-bg-4', 0, '2020-02-17 09:47:38', '2020-02-29 22:35:10'),
(5, 'Security Services', 'fa fa-briefcase icon-bg-5', 1, '2020-02-29 22:29:29', '2020-02-29 22:42:21'),
(6, 'Vehicles & Machines', 'fa fa-car icon-bg-6', 1, '2020-02-29 22:29:29', '2020-02-29 22:42:24'),
(7, 'Electronics', 'fa fa-paw icon-bg-7', 1, '2020-02-29 22:29:41', '2020-02-29 22:42:28'),
(8, 'Services', 'fa fa-laptop icon-bg-8', 1, '2020-02-29 22:29:41', '2020-02-29 22:42:31');

-- --------------------------------------------------------

--
-- Table structure for table `propertyfees`
--

CREATE TABLE `propertyfees` (
  `id` int(11) NOT NULL,
  `propertyId` int(11) NOT NULL,
  `feeId` int(5) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `propertypaymentdeadlines`
--

CREATE TABLE `propertypaymentdeadlines` (
  `id` int(11) NOT NULL,
  `propertyId` int(11) NOT NULL,
  `paymentdeadlineId` int(5) NOT NULL,
  `days` int(2) DEFAULT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `propertyphotos`
--

CREATE TABLE `propertyphotos` (
  `id` int(11) NOT NULL,
  `propertyId` int(11) NOT NULL,
  `photo` varchar(50) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `propertyphotos`
--

INSERT INTO `propertyphotos` (`id`, `propertyId`, `photo`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 9, '15829563910.jpg', 1, '2020-02-29 03:06:31', '2020-02-29 14:43:17'),
(2, 9, '15829563911.jpg', 0, '2020-02-29 03:06:31', '2020-02-29 03:06:31'),
(3, 9, '15829563912.jpg', 0, '2020-02-29 03:06:31', '2020-02-29 03:06:31'),
(4, 9, '15829563913.jpg', 0, '2020-02-29 03:06:31', '2020-02-29 03:06:31'),
(5, 9, '1582997657.jpg', 0, '2020-02-29 14:34:17', '2020-02-29 14:34:17'),
(6, 10, '15830125720.jpg', 0, '2020-02-29 18:42:52', '2020-02-29 18:42:52'),
(7, 10, '15830125721.jpg', 0, '2020-02-29 18:42:52', '2020-02-29 18:42:52'),
(8, 10, '15830125722.jpg', 0, '2020-02-29 18:42:52', '2020-02-29 18:42:52'),
(9, 10, '15830125723.jpg', 0, '2020-02-29 18:42:52', '2020-02-29 18:42:52'),
(10, 11, '15836042730.png', 0, '2020-03-07 15:04:33', '2020-03-07 15:04:33'),
(11, 11, '15836042731.png', 0, '2020-03-07 15:04:33', '2020-03-07 15:04:33'),
(12, 11, '15836042732.jpg', 0, '2020-03-07 15:04:33', '2020-03-07 15:04:33'),
(13, 11, '15836042733.jpg', 0, '2020-03-07 15:04:33', '2020-03-07 15:04:33'),
(14, 12, '15838461960.jpg', 0, '2020-03-10 10:16:36', '2020-03-10 10:16:36'),
(15, 12, '15838461961.jpg', 0, '2020-03-10 10:16:37', '2020-03-10 10:16:37'),
(16, 12, '15838461972.jpg', 0, '2020-03-10 10:16:37', '2020-03-10 10:16:37'),
(17, 12, '15838461973.jpg', 0, '2020-03-10 10:16:37', '2020-03-10 10:16:37'),
(18, 13, 'RentPoa_Rent_Poa_Photo_Single Room_15839070520.jpg', 0, '2020-03-11 06:10:52', '2020-03-11 06:10:52'),
(19, 13, 'RentPoa_Rent_Poa_Photo_Single Room_15839070521.jpg', 0, '2020-03-11 06:10:52', '2020-03-11 06:10:52'),
(20, 13, 'RentPoa_Rent_Poa_Photo_Single Room_15839070522.jpg', 0, '2020-03-11 06:10:52', '2020-03-11 06:10:52'),
(21, 13, 'RentPoa_Rent_Poa_Photo_Single Room_15839070523.jpg', 0, '2020-03-11 06:10:52', '2020-03-11 06:10:52'),
(22, 14, 'RentPoa_Rent_Poa_Photo_Bedsitter_15839076170.jpg', 0, '2020-03-11 06:20:17', '2020-03-11 06:20:17'),
(23, 14, 'RentPoa_Rent_Poa_Photo_Bedsitter_15839076171.jpg', 0, '2020-03-11 06:20:17', '2020-03-11 06:20:17'),
(24, 14, 'RentPoa_Rent_Poa_Photo_Bedsitter_15839076172.jpg', 0, '2020-03-11 06:20:17', '2020-03-11 06:20:17'),
(25, 14, 'RentPoa_Rent_Poa_Photo_Bedsitter_15839076173.jpg', 0, '2020-03-11 06:20:17', '2020-03-11 06:20:17');

-- --------------------------------------------------------

--
-- Table structure for table `propertyproximities`
--

CREATE TABLE `propertyproximities` (
  `id` int(11) NOT NULL,
  `propertyId` int(11) NOT NULL,
  `proximityId` int(5) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `propertysection`
--

CREATE TABLE `propertysection` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `propertysection`
--

INSERT INTO `propertysection` (`id`, `name`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'For Rent', 0, '2020-02-17 13:49:16', '2020-02-17 13:49:16'),
(2, 'For Sale', 0, '2020-02-17 13:49:16', '2020-02-17 13:49:16');

-- --------------------------------------------------------

--
-- Table structure for table `propertysitevisits`
--

CREATE TABLE `propertysitevisits` (
  `id` int(11) NOT NULL,
  `propertyId` int(11) NOT NULL,
  `sitevisitId` int(5) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `propertytypes`
--

CREATE TABLE `propertytypes` (
  `id` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `typeName` varchar(50) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `propertytypes`
--

INSERT INTO `propertytypes` (`id`, `categoryId`, `typeName`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Apartment', 0, '2020-02-17 13:48:49', '2020-02-29 04:44:30'),
(2, 1, 'Bedsitter', 0, '2020-02-17 13:48:49', '2020-02-17 13:48:49'),
(3, 1, 'Bungalow', 0, '2020-02-29 04:46:49', '2020-02-29 04:46:49'),
(4, 1, '1 Bedroom', 0, '2020-02-29 04:46:49', '2020-02-29 04:51:53'),
(5, 1, 'Single Room', 0, '2020-02-29 04:52:30', '2020-02-29 04:52:30'),
(6, 1, '2 Bedroom', 0, '2020-02-29 04:52:30', '2020-02-29 04:52:30'),
(7, 1, '3 Bedroom', 0, '2020-02-29 04:53:37', '2020-02-29 04:53:37');

-- --------------------------------------------------------

--
-- Table structure for table `proximities`
--

CREATE TABLE `proximities` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proximities`
--

INSERT INTO `proximities` (`id`, `name`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'School', 0, '2020-02-19 07:19:09', '2020-02-19 07:29:32'),
(2, 'Church', 0, '2020-02-19 07:19:09', '2020-02-19 07:29:32'),
(3, 'Hospital', 0, '2020-02-19 07:19:29', '2020-02-19 07:29:32'),
(4, 'Police Station', 0, '2020-02-19 07:19:29', '2020-02-19 07:30:51'),
(5, 'Main Road', 0, '2020-02-19 07:29:50', '2020-02-19 07:29:50'),
(6, 'Shopping Mall', 0, '2020-02-19 07:29:50', '2020-02-19 07:30:56');

-- --------------------------------------------------------

--
-- Table structure for table `sitevisits`
--

CREATE TABLE `sitevisits` (
  `id` int(11) NOT NULL,
  `name` varchar(10) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sitevisits`
--

INSERT INTO `sitevisits` (`id`, `name`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Any Day', 0, '2020-02-19 07:19:09', '2020-02-19 07:19:09'),
(2, 'Monday', 0, '2020-02-19 07:19:09', '2020-02-19 07:19:09'),
(3, 'Weekdays', 0, '2020-02-19 07:19:29', '2020-02-19 07:19:29'),
(4, 'Weekend', 0, '2020-02-19 07:19:29', '2020-02-19 07:19:29');

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE `sms` (
  `id` int(11) NOT NULL,
  `mobileNo` varchar(20) NOT NULL,
  `message` text NOT NULL,
  `sent` tinyint(3) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `subcounties`
--

CREATE TABLE `subcounties` (
  `id` int(11) NOT NULL,
  `countyId` int(11) NOT NULL,
  `subCountyName` varchar(20) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subcounties`
--

INSERT INTO `subcounties` (`id`, `countyId`, `subCountyName`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ngara', 0, '2020-02-07 13:43:25', '2020-02-07 13:43:25'),
(2, 1, 'Embakasi', 0, '2020-02-07 13:43:25', '2020-02-07 13:43:25'),
(3, 1, 'Thika Road', 0, '2020-02-29 20:05:34', '2020-02-29 20:05:34'),
(4, 1, 'South B', 0, '2020-02-29 20:05:34', '2020-02-29 20:05:34'),
(5, 1, 'Ongata Romgai', 0, '2020-02-29 20:05:56', '2020-02-29 20:05:56'),
(6, 1, 'Kawangware', 0, '2020-02-29 20:05:56', '2020-02-29 20:05:56'),
(7, 1, 'Karen', 0, '2020-02-29 20:06:39', '2020-02-29 20:06:39'),
(8, 1, 'Huruma', 0, '2020-02-29 20:06:39', '2020-02-29 20:06:39'),
(9, 1, 'Lavington', 0, '2020-02-29 20:07:17', '2020-02-29 20:07:17'),
(10, 1, 'Umoja', 0, '2020-02-29 20:07:17', '2020-02-29 20:07:17');

-- --------------------------------------------------------

--
-- Table structure for table `targets`
--

CREATE TABLE `targets` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `targets`
--

INSERT INTO `targets` (`id`, `name`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'All', 0, '2020-02-19 07:16:20', '2020-02-19 07:16:20'),
(2, 'Students', 0, '2020-02-19 07:16:20', '2020-02-19 07:16:20'),
(3, 'Couples', 0, '2020-02-19 07:16:45', '2020-02-19 07:16:45'),
(4, 'Middle Class', 0, '2020-02-19 07:16:45', '2020-02-19 07:16:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `accountType` tinyint(3) NOT NULL DEFAULT '1',
  `roleId` tinyint(3) NOT NULL DEFAULT '1',
  `mobileNo` varchar(20) NOT NULL,
  `isOnline` tinyint(3) NOT NULL DEFAULT '0',
  `isBlocked` tinyint(3) NOT NULL DEFAULT '0',
  `isVetted` tinyint(3) NOT NULL DEFAULT '0',
  `notifyMe` tinyint(3) NOT NULL DEFAULT '1',
  `profilepic` varchar(50) NOT NULL DEFAULT 'profile.jpg',
  `reset_code` varchar(100) DEFAULT NULL,
  `confirmpassword` varchar(100) DEFAULT NULL,
  `fcmToken` text,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `isVerified` tinyint(3) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstName`, `lastName`, `email`, `password`, `accountType`, `roleId`, `mobileNo`, `isOnline`, `isBlocked`, `isVetted`, `notifyMe`, `profilepic`, `reset_code`, `confirmpassword`, `fcmToken`, `status`, `isVerified`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Brian', 'Mulunda', 'medevkenya@gmail.com', '$2y$10$OIzNb.Elcx2AD3uuHSovfOgPm91ICGKYrdq714r.qEViEApoZi0iC', 1, 1, '254702066508', 1, 0, 1, 1, '69976.jpg', 'e2a352fc3458cefcad55837ff8ce1d79', NULL, NULL, 1, 1, 0, '2020-02-13 05:35:00', '2020-03-06 18:45:19'),
(2, 'William', 'Ontomu', 'williamontomu@gmail.com', '$2y$10$FyP2/r7tAPoUbFkcuKuZH.VGvDUDN9ZFfXvaxpD2KHTr1VUp8ILaG', 1, 1, '0725142514', 0, 0, 1, 1, 'profile1.jpg', '848803abb1915409514407f69c3c6a19', NULL, NULL, 0, 0, 0, '2020-02-13 05:36:40', '2020-02-14 08:36:02'),
(3, 'Brian', 'Mulunda', 'brian@augusta.co.ke', '$2y$10$m0va5fP3jK0OQMptNnhCSut42bhP/M.h7Hf4pBckmlgFlLGDDABUa', 1, 1, '254712970610', 1, 0, 0, 1, 'profile.jpg', 'fda798a6a9aaa97deb7ddb4c29cbe852', NULL, NULL, 1, 0, 0, '2020-02-20 16:29:50', '2020-03-10 13:31:06'),
(4, 'ffffff', 'eeeee', 'admin@admin.com', '$2y$10$e/hZzP3wwlHEdOy4wudNhu9AUy2234MyCDVfNnoBIqPgZGpuqP7tK', 1, 1, '254765555555', 0, 0, 0, 1, 'profile.png', '55517018e6e73428cbabd589de3c32eb', NULL, NULL, 0, 0, 0, '2020-02-28 16:22:23', '2020-02-28 16:22:23'),
(5, 'gggggggggg', 'nnnnnnnnn', 'admin@gmail.com', '$2y$10$jfbkOGA6hy4N7RiXTqS3EeYES7cRibEHWQLXCpdb4LsTadrv8eIWW', 1, 1, '254725145412', 1, 0, 0, 1, 'profile.png', 'fab2c9abbcc1e6d30a0d58df94e9351b', NULL, NULL, 0, 0, 0, '2020-02-28 16:23:54', '2020-02-28 16:24:21'),
(9, 'Johnson', 'Kimemia', 'brianmulunda92@gmail.com', '$2y$10$XV8NRbKRxett1yBTDzS3EeXJi10dIkeKCYmU.8hHVFG1E3YvNzng6', 1, 1, '254714254578', 1, 0, 0, 1, '33651.png', '0', NULL, NULL, 1, 0, 0, '2020-03-11 05:42:32', '2020-03-11 06:31:03'),
(10, 'David', 'Musyoka', 'davidmucioca@gmail.com', '$2y$10$52ojIjvaDy2fUQ4X1ivySOFlxmOQpwk9Lm6.v5/doCfAW7CkV5w0i', 1, 1, '254701541677', 1, 0, 0, 1, '45424.jpg', '0', NULL, NULL, 1, 0, 0, '2020-03-11 08:19:27', '2020-03-11 09:39:32');

-- --------------------------------------------------------

--
-- Table structure for table `waterplans`
--

CREATE TABLE `waterplans` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `waterplans`
--

INSERT INTO `waterplans` (`id`, `name`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Everyday', 0, '2020-02-19 07:04:12', '2020-02-19 07:04:12'),
(2, 'Certain Days', 0, '2020-02-19 07:04:12', '2020-02-19 07:04:12'),
(3, 'Certain Hours', 0, '2020-02-19 07:04:25', '2020-02-19 07:04:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `counties`
--
ALTER TABLE `counties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customerpropertycontacts`
--
ALTER TABLE `customerpropertycontacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposittypes`
--
ALTER TABLE `deposittypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emaillogs`
--
ALTER TABLE `emaillogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `managementtypes`
--
ALTER TABLE `managementtypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propertycategories`
--
ALTER TABLE `propertycategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propertyfees`
--
ALTER TABLE `propertyfees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propertypaymentdeadlines`
--
ALTER TABLE `propertypaymentdeadlines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propertyphotos`
--
ALTER TABLE `propertyphotos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propertyproximities`
--
ALTER TABLE `propertyproximities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propertysection`
--
ALTER TABLE `propertysection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propertysitevisits`
--
ALTER TABLE `propertysitevisits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propertytypes`
--
ALTER TABLE `propertytypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proximities`
--
ALTER TABLE `proximities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sitevisits`
--
ALTER TABLE `sitevisits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcounties`
--
ALTER TABLE `subcounties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `targets`
--
ALTER TABLE `targets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `waterplans`
--
ALTER TABLE `waterplans`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `counties`
--
ALTER TABLE `counties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customerpropertycontacts`
--
ALTER TABLE `customerpropertycontacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deposittypes`
--
ALTER TABLE `deposittypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `emaillogs`
--
ALTER TABLE `emaillogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `managementtypes`
--
ALTER TABLE `managementtypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `propertycategories`
--
ALTER TABLE `propertycategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `propertyfees`
--
ALTER TABLE `propertyfees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `propertypaymentdeadlines`
--
ALTER TABLE `propertypaymentdeadlines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `propertyphotos`
--
ALTER TABLE `propertyphotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `propertyproximities`
--
ALTER TABLE `propertyproximities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `propertysection`
--
ALTER TABLE `propertysection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `propertysitevisits`
--
ALTER TABLE `propertysitevisits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `propertytypes`
--
ALTER TABLE `propertytypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `proximities`
--
ALTER TABLE `proximities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sitevisits`
--
ALTER TABLE `sitevisits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subcounties`
--
ALTER TABLE `subcounties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `targets`
--
ALTER TABLE `targets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `waterplans`
--
ALTER TABLE `waterplans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
