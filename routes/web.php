<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|https://hdtuto.com/article/laravel-56-registration-form-validation-with-error-messages
/https://hdtuto.com/article/laravel-56-login-with-google-with-socialite
/https://hdtuto.com/article/php-ajax-autocomplete-search-from-database-example
/https://hdtuto.com/article/laravel-56-registration-form-validation-with-error-messages
/https://hdtuto.com/article/laravel-56-multiple-file-upload-with-validation-example
/https://hdtuto.com/article/image-upload-with-validation-in-php-laravel-56
/https://hdtuto.com/article/multiple-file-upload-in-laravel-6-example
/https://hdtuto.com/article/ajax-validation-in-laravel-57-example
/https://hdtuto.com/article/laravel-6-check-file-exists-in-folder-example
*/

Route::get('/', function () {
    return view('frontend.rentpoa');
});

Route::get('/', 'RentpoaController@rentpoa');
Route::get('searchHome', 'RentpoaController@rentpoa');
Route::get('searchCategory', 'RentpoaController@rentpoa');
Route::get('searchType', 'RentpoaController@rentpoa');
Route::get('searchLocation', 'RentpoaController@rentpoa');

Route::post('searchHome', 'RentpoaController@searchHome');
Route::post('/searchCategory', 'RentpoaController@searchCategory');
Route::post('/searchType', 'RentpoaController@searchType');
Route::post('/searchLocation', 'RentpoaController@searchLocation');

Route::get('/login', 'RentpoaController@login');
Route::post('doLogin', 'RentpoaController@doLogin');
Route::get('login', [ 'as' => 'login', 'uses' => 'RentpoaController@Login']);
Route::get('/register', 'RentpoaController@register');
Route::post('doRegister', 'RentpoaController@doRegister');
Route::get('/logout', 'RentpoaController@logout');
Route::get('/activateAccount', 'RentpoaController@activateAccount');

Route::get('/getCategories', 'RentpoaController@getCategories');
Route::get('/getLocations', 'RentpoaController@getLocations');

Route::get('resetPasswordLink/{PasswordConfirmation}/{email}', 'ForgotpasswordController@resetPasswordLink');
Route::get('forgotPassword', 'ForgotpasswordController@forgotPassword');
Route::post('resetPassword', 'ForgotpasswordController@resetPassword');
Route::post('doResetPassword', 'ForgotpasswordController@doResetPassword');

Route::get('/aboutRentPoa', 'RentpoaController@aboutRentPoa');
Route::get('/contactRentPoa', 'RentpoaController@contactRentPoa');
Route::get('/termsOfService', 'RentpoaController@termsOfService');
Route::get('/howRentPoaWorks', 'RentpoaController@howRentPoaWorks');

Route::get('/latestRentals', 'RentpoaController@latestRentals');
Route::get('/popularRentals', 'RentpoaController@popularRentals');

Route::get('/addrental', 'RentpoaController@addrental');
Route::post('postRental', 'RentpoaController@postRental')->name('postRental');
Route::get('/coreShareRent', 'CoreshareController@coreShareRent');

Route::get('/viewByCategory/{categoryId}', 'ViewsController@viewByCategory');

Route::get('/viewByType/{categoryId}', 'ViewsController@viewByType');
Route::get('/viewByLocation/{subCountyId}', 'ViewsController@viewByLocation');
Route::get('/viewRent/{Id}', 'ViewsController@viewRent');

Route::get('/viewByManagementType/{typeId}', 'ViewsController@viewByManagementType');
Route::get('/viewByTarget/{targetId}', 'ViewsController@viewByTarget');
Route::get('/viewByDepositType/{depositTypeId}', 'ViewsController@viewByDepositType');
Route::get('/viewByFeeType/{feeTypeId}', 'ViewsController@viewByFeeType');
Route::get('/viewByProximity/{proximityId}', 'ViewsController@viewByProximity');
Route::get('/viewWithBedrooms/{Id}', 'ViewsController@viewWithBedrooms');
Route::get('/viewWithParking/{Id}', 'ViewsController@viewWithParking');
Route::get('/viewByFloor/{Id}', 'ViewsController@viewByFloor');
Route::get('/viewWithBalcon/{Id}', 'ViewsController@viewWithBalcon');
Route::get('/viewGated/{Id}', 'ViewsController@viewGated');
Route::get('/viewWithElectricFence/{Id}', 'ViewsController@viewWithElectricFence');
Route::get('/viewWithSecurity/{Id}', 'ViewsController@viewWithSecurity');
Route::get('/viewWithCctv/{Id}', 'ViewsController@viewWithCctv');
Route::get('/viewAllowedPets/{Id}', 'ViewsController@viewAllowedPets');
Route::get('/viewWithGenerator/{Id}', 'ViewsController@viewWithGenerator');
Route::get('/viewWithElevator/{Id}', 'ViewsController@viewWithElevator');
Route::get('/viewWithPlayground/{Id}', 'ViewsController@viewWithPlayground');
Route::get('/viewFurnished/{Id}', 'ViewsController@viewFurnished');

Route::get('getRentalTypes', 'ListingController@getRentalTypes')->name('getRentalTypes');
Route::get('getSubcounties', 'ListingController@getSubcounties')->name('getSubcounties');

Route::get('submitPhone', 'RentpoaController@submitPhone');
Route::get('verifyCode', 'RentpoaController@verifyCode');

Route::group(['middleware' => 'auth'], function () {
Route::get('dashboard', 'DashboardController@Dashboard');
Route::get('changepassword', 'DashboardController@changepassword');
Route::post('dochangepassword', 'DashboardController@dochangepassword');
Route::get('profile', 'DashboardController@profile');
Route::post('updateProfile', 'DashboardController@updateProfile');
Route::get('mypic', 'DashboardController@mypic');
Route::post('editPic', 'DashboardController@editPic')->name('editPic');
Route::get('error', 'DashboardController@ErrorPage');

Route::get('/addListing', 'ListingController@addListing');
Route::post('postListing', 'ListingController@postListing')->name('postListing');
Route::get('/viewRental/{Id}', 'ListingController@viewRental');
Route::post('addPhoto', 'ListingController@addPhoto')->name('addPhoto');
Route::get('/removePhoto/{Id}', 'ListingController@removePhoto');
Route::get('/editRental/{Id}', 'ListingController@editRental');
Route::post('doEditRental', 'ListingController@doEditRental')->name('doEditRental');
Route::get('/removeRental/{Id}', 'ListingController@removeRental');

});
