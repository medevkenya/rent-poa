<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Register</title>

@include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.top')

<!--==================================
=            User Profile            =
===================================-->

<section class="user-profile section">
	<div class="container">
		<div class="row">

			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Edit Personal Info -->
				<div class="widget personal-info">
          <h2>Create Account</h2>
					<h3 class="widget-header user">Fill registration form below</h3>

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
          @endif

            {!! Form::open(['url'=>'doRegister']) !!}
            {{ csrf_field() }}
						<!-- First Name -->
						<div class="form-group">
						    <label for="first-name">First Name</label>
						    <input type="text" class="form-control" name="firstName" id="first-name"  value="{{ old('firstName') }}">
                @if ($errors->has('firstName'))
                    <span class="text-danger">{{ $errors->first('firstName') }}</span>
                @endif
						</div>
						<!-- Last Name -->
						<div class="form-group {{ $errors->has('lastName') ? 'has-error' : '' }}">
						    <label for="last-name">Last Name</label>
						    <input type="text" class="form-control" name="lastName" id="last-name"  value="{{ old('lastName') }}">
                @if ($errors->has('lastName'))
                    <span class="text-danger">{{ $errors->first('lastName') }}</span>
                @endif
						</div>
						<div class="form-group {{ $errors->has('mobileNo') ? 'has-error' : '' }}">
						    <label for="comunity-name">Mobile Number</label>
						    <input type="text" class="form-control" name="mobileNo" id="mobileNo"  value="{{ old('mobileNo') }}">
                @if ($errors->has('mobileNo'))
                    <span class="text-danger">{{ $errors->first('mobileNo') }}</span>
                @endif
						</div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
						    <label for="comunity-name">Email Address</label>
						    <input type="email" class="form-control" name="email" id="email"  value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
						</div>

            <div class="form-group">
						    <label for="current-password {{ $errors->has('password') ? 'has-error' : '' }}">Password</label>
						    <input type="password" name="password" class="form-control" id="password">
                @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
						</div>

            <!-- Checkbox -->
						<div class="form-check {{ $errors->has('terms') ? 'has-error' : '' }}">
						  <label class="form-check-label" for="hide-profile">
						    <input class="form-check-input" name="terms" type="checkbox" value="1" id="hide-profile">
						    I have read Rent Poa's <a href="{{URL::to('/termsOfService')}}" class="linkref">terms of use</a>
						  </label>
              @if ($errors->has('terms'))
                  <span class="text-danger">{{ $errors->first('terms') }}</span>
              @endif
						</div>

						<!-- Submit button -->
						<button class="btn btn-main" type="submit">Register</button>
					{!! Form::close() !!}
				</div>

			</div>

      @include('frontend.rightbar')

		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

</body>

</html>
