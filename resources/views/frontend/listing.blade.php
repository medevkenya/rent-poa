<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Add Rental</title>

  @include('frontend.headerlinks')

  <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script> -->

</head>

<body class="body-wrapper">

@include('frontend.topinner')

<!--==================================
=            User Profile            =
===================================-->

<section class="user-profile section">
	<div class="container">
		<div class="row">

			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Edit Personal Info -->
				<div class="widget personal-info">
          <h2>Add Rental</h2>
					<h3 class="widget-header user">Fill the form below to list your rental</h3>

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
          @endif

            <form action="{{ route('postListing') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
            <!-- <div class="form-group col-md-4">
						    <label for="first-name">Listing For</label>
						    <select class="form-control" name="propertySectionId" id="propertySectionId" required>
                  <option>Select</option>
                  <?php
                  //$sections = App\Rentalsection::getAll();
                  //foreach ($sections as $keysec) {
                  ?>
                  <option value="<?php //echo $keysec->id; ?>"><?php //echo $keysec->name; ?></option>
                  <?php //} ?>
                </select>
                @if ($errors->has('propertySectionId'))
                    <span class="text-danger">{{ $errors->first('propertySectionId') }}</span>
                @endif
						</div> -->

            <input type="hidden" name="propertySectionId" value="1">

            <div class="form-group col-md-6">
						    <label for="first-name">Select Category</label>
						    <select class="form-control" name="categoryId" id="categoryId" required>
                  <option>Select</option>
                  <?php
                  $categories = App\Propertycategories::getAll();
                  foreach ($categories as $key) {
                  ?>
                  <option value="<?php echo $key->id; ?>"><?php echo $key->categoryName; ?></option>
                  <?php } ?>
                </select>
                @if ($errors->has('categoryId'))
                    <span class="text-danger">{{ $errors->first('categoryId') }}</span>
                @endif
						</div>

            <div class="form-group col-md-6">
						    <label for="first-name">Select Type</label>
						    <select class="form-control" name="propertyTypeId" id="propertytypes" required>
                  <option>Select</option>
                </select>
                @if ($errors->has('propertyTypeId'))
                    <span class="text-danger">{{ $errors->first('propertyTypeId') }}</span>
                @endif
						</div>

            <div class="form-group  col-md-12">
						    <label for="first-name">Title / Name of property</label>
						    <input type="text" class="form-control" name="title" id="title"  value="{{ old('title') }}" required>
                @if ($errors->has('title'))
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                @endif
						</div>

						<!-- <div class="form-group">
						    <label for="first-name">Title / Name of property</label>
						    <input type="text" class="form-control" name="title" id="first-name"  value="{{ old('title') }}">
                @if ($errors->has('title'))
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                @endif
						</div> -->

						<!-- <div class="form-group {{ $errors->has('location') ? 'has-error' : '' }}">
						    <label for="last-name">Location</label>
						    <input type="text" class="form-control" name="location" id="last-name"  value="{{ old('location') }}">
                @if ($errors->has('location'))
                    <span class="text-danger">{{ $errors->first('location') }}</span>
                @endif
						</div> -->

            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}  col-md-12">
						    <label for="comunity-name">Description</label>
						    <input type="text" class="form-control" name="description" id="description"  value="{{ old('description') }}" required>
                @if ($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
						</div>

            <div class="form-group  col-md-6">
						    <label for="first-name">Select County</label>
						    <select class="form-control" name="countyId" id="countyId" required>
                  <option>Select</option>
                  <?php $counties = App\Counties::getAll();
                  foreach ($counties as $keyco) { ?>
                  <option value="<?php echo $keyco->id; ?>"><?php echo $keyco->countyName; ?></option>
                  <?php } ?>
                </select>
                @if ($errors->has('countyId'))
                    <span class="text-danger">{{ $errors->first('countyId') }}</span>
                @endif
						</div>

            <div class="form-group  col-md-6">
						    <label for="first-name">Select Location</label>
						    <select class="form-control" name="subCountyId" id="subCountyId" required>
                  <option>Select</option>
                </select>
                @if ($errors->has('subCountyId'))
                    <span class="text-danger">{{ $errors->first('subCountyId') }}</span>
                @endif
						</div>

            <div class="form-group {{ $errors->has('directions') ? 'has-error' : '' }}  col-md-12">
						    <label for="comunity-name">Directions</label>
						    <input type="text" class="form-control" name="directions" id="directions"  value="{{ old('directions') }}" required>
                @if ($errors->has('directions'))
                    <span class="text-danger">{{ $errors->first('directions') }}</span>
                @endif
						</div>

            <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }} col-md-4" id="amount">
               <label for="comunity-name">Rent/Cost (Ksh.)</label>
               <input type="number" class="form-control" name="amount"  value="{{ old('amount') }}" required>
                @if ($errors->has('amount'))
                    <span class="text-danger">{{ $errors->first('amount') }}</span>
                @endif
           </div>

            <!-- File chooser -->
						<div class="form-group choose-file {{ $errors->has('uploadFile') ? 'has-error' : '' }}  col-md-12">
              <label for="uploading-name">Photos (At least 4, Maximum 9)</label>
							<i class="fa fa-user text-center"></i>
						    <input type="file" id="uploadFile" name="uploadFile[]" multiple class="form-control-file d-inline" id="input-file" required>
                @if ($errors->has('uploadFile'))
                    <span class="text-danger">{{ $errors->first('uploadFile') }}</span>
                @endif
						 </div>

             <br/>
             <div id="image_preview"></div>

             <div class=" col-md-12"></div>

             <div class="form-group {{ $errors->has('bedrooms') ? 'has-error' : '' }} hiddenElement col-md-4" id="bedrooms">
 						    <label for="comunity-name">Bedrooms</label>
 						    <input type="text" class="form-control" name="bedrooms"  value="{{ old('bedrooms') }}">
                 @if ($errors->has('bedrooms'))
                     <span class="text-danger">{{ $errors->first('bedrooms') }}</span>
                 @endif
 						</div>

             <div class="form-group {{ $errors->has('bathrooms') ? 'has-error' : '' }} hiddenElement col-md-4" id="bathrooms">
 						    <label for="comunity-name">Bathrooms</label>
 						    <input type="text" class="form-control" name="bathrooms"  value="{{ old('bathrooms') }}">
                 @if ($errors->has('bathrooms'))
                     <span class="text-danger">{{ $errors->first('bathrooms') }}</span>
                 @endif
 						</div>

            <div class="col-md-12 hiddenElement" id="extraOptions"><p>Kindly tick (&#10004;) where applicable</p></div>

            <div class="form-group {{ $errors->has('propertySize') ? 'has-error' : '' }} hiddenElement col-md-4" id="propertySize">
               <label for="comunity-name">Rental Size</label>
               <input type="text" class="form-control" name="propertySize"  value="{{ old('propertySize') }}">
                @if ($errors->has('propertySize'))
                    <span class="text-danger">{{ $errors->first('propertySize') }}</span>
                @endif
           </div>

           <div class=" col-md-12"></div>

             <div class="form-check {{ $errors->has('gated') ? 'has-error' : '' }} hiddenElement col-md-4" id="gated">
 						  <label class="form-check-label" for="hide-profile">
 						    <input class="form-check-input" name="gated" type="checkbox" value="1">
 						    Is Gated
 						  </label>
               @if ($errors->has('gated'))
                   <span class="text-danger">{{ $errors->first('gated') }}</span>
               @endif
 						</div>

            <div class="form-check {{ $errors->has('hasBalcony') ? 'has-error' : '' }} hiddenElement col-md-4" id="hasBalcony">
             <label class="form-check-label" for="hide-profile">
               <input class="form-check-input" name="hasBalcony" type="checkbox" value="1">
               Has Balcon
             </label>
              @if ($errors->has('hasBalcony'))
                  <span class="text-danger">{{ $errors->first('hasBalcony') }}</span>
              @endif
           </div>

           <div class="form-check {{ $errors->has('electricfence') ? 'has-error' : '' }} hiddenElement col-md-4" id="electricfence">
            <label class="form-check-label" for="hide-profile">
              <input class="form-check-input" name="electricfence" type="checkbox" value="1">
              Has Electric Fence
            </label>
             @if ($errors->has('electricfence'))
                 <span class="text-danger">{{ $errors->first('electricfence') }}</span>
             @endif
          </div>

          <div class="form-check {{ $errors->has('swimmingpool') ? 'has-error' : '' }} hiddenElement col-md-4" id="swimmingpool">
           <label class="form-check-label" for="hide-profile">
             <input class="form-check-input" name="swimmingpool" type="checkbox" value="1">
             Has Swimming Pool
           </label>
            @if ($errors->has('swimmingpool'))
                <span class="text-danger">{{ $errors->first('swimmingpool') }}</span>
            @endif
         </div>

         <div class="form-check {{ $errors->has('furnished') ? 'has-error' : '' }} hiddenElement col-md-4" id="furnished">
          <label class="form-check-label" for="hide-profile">
            <input class="form-check-input" name="furnished" type="checkbox" value="1">
            Is Furnished
          </label>
           @if ($errors->has('furnished'))
               <span class="text-danger">{{ $errors->first('furnished') }}</span>
           @endif
        </div>

        <div class="form-check {{ $errors->has('liftelevator') ? 'has-error' : '' }} hiddenElement col-md-4" id="liftelevator">
         <label class="form-check-label" for="hide-profile">
           <input class="form-check-input" name="liftelevator" type="checkbox" value="1">
           Has Elevator
         </label>
          @if ($errors->has('liftelevator'))
              <span class="text-danger">{{ $errors->first('liftelevator') }}</span>
          @endif
       </div>

       <div class="form-check {{ $errors->has('playground') ? 'has-error' : '' }} hiddenElement col-md-4" id="playground">
        <label class="form-check-label" for="hide-profile">
          <input class="form-check-input" name="playground" type="checkbox" value="1">
          Has Playground
        </label>
         @if ($errors->has('playground'))
             <span class="text-danger">{{ $errors->first('playground') }}</span>
         @endif
      </div>

      <div class="form-check {{ $errors->has('negotiable') ? 'has-error' : '' }} hiddenElement col-md-4" id="negotiable">
       <label class="form-check-label" for="hide-profile">
         <input class="form-check-input" name="negotiable" type="checkbox" value="1">
         Negotiable Rent
       </label>
        @if ($errors->has('negotiable'))
            <span class="text-danger">{{ $errors->first('negotiable') }}</span>
        @endif
     </div>

     <div class="form-check {{ $errors->has('backupgenerator') ? 'has-error' : '' }} hiddenElement col-md-4" id="backupgenerator">
      <label class="form-check-label" for="hide-profile">
        <input class="form-check-input" name="backupgenerator" type="checkbox" value="1">
        Has Backup Generator
      </label>
       @if ($errors->has('backupgenerator'))
           <span class="text-danger">{{ $errors->first('backupgenerator') }}</span>
       @endif
    </div>

    <div class="form-check {{ $errors->has('cctv') ? 'has-error' : '' }} hiddenElement col-md-4" id="cctv">
     <label class="form-check-label" for="hide-profile">
       <input class="form-check-input" name="cctv" type="checkbox" value="1">
       Has CCTV Cameras
     </label>
      @if ($errors->has('cctv'))
          <span class="text-danger">{{ $errors->first('cctv') }}</span>
      @endif
   </div>

   <div class="form-check {{ $errors->has('pets') ? 'has-error' : '' }} hiddenElement col-md-4" id="pets">
    <label class="form-check-label" for="hide-profile">
      <input class="form-check-input" name="pets" type="checkbox" value="1">
      Pets are allowed
    </label>
     @if ($errors->has('pets'))
         <span class="text-danger">{{ $errors->first('pets') }}</span>
     @endif
  </div>

  <div class="form-check {{ $errors->has('security') ? 'has-error' : '' }} hiddenElement col-md-4" id="security">
   <label class="form-check-label" for="hide-profile">
     <input class="form-check-input" name="security" type="checkbox" value="1">
     Has Security Guard(s)
   </label>
    @if ($errors->has('security'))
        <span class="text-danger">{{ $errors->first('security') }}</span>
    @endif
 </div>

 <div class="form-check {{ $errors->has('parking') ? 'has-error' : '' }} hiddenElement col-md-4" id="parking">
  <label class="form-check-label" for="hide-profile">
    <input class="form-check-input" name="parking" type="checkbox" value="1">
    Has Parking Space(s)
  </label>
   @if ($errors->has('parking'))
       <span class="text-danger">{{ $errors->first('parking') }}</span>
   @endif
</div>

          </div>
						<!-- Submit button -->
						<button class="btn btn-main" type="submit">Add</button>
					{!! Form::close() !!}
				</div>

			</div>

      @include('frontend.rightbarinner')

		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

@include('frontend.footerlinks')

<script type="text/javascript">
  $("#uploadFile").change(function(){
    document.getElementById("image_preview").style.display = "block";
     $('#image_preview').html("");
     var total_file=document.getElementById("uploadFile").files.length;
     for(var i=0;i<total_file;i++)
     {
      $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
     }
  });

  $('form').ajaxForm(function()
   {
    alert("Uploaded SuccessFully");
   });
</script>

<script type="text/javascript">
   $("#categoryId").change(function(){
       $.ajax({
           url: "{{ route('getRentalTypes') }}?categoryId=" + $(this).val(),
           method: 'GET',
           success: function(data) {
               $('#propertytypes').html(data.html);
           }
       });
   });
   $("#countyId").change(function(){
       $.ajax({
           url: "{{ route('getSubcounties') }}?countyId=" + $(this).val(),
           method: 'GET',
           success: function(data) {
               $('#subCountyId').html(data.html);
           }
       });
   });
   $("#propertytypes").change(function(){
     var propertyTypeId = $(this).val();
     //console.log("propertyTypeId--"+propertyTypeId);
       if(propertyTypeId == 1 || propertyTypeId == 3) { //Apartment / Bungalow
         $('#extraOptions').show();
         $('#gated').show();
         $('#hasBalcony').show();
         $('#swimmingpool').show();
         $('#playground').show();
         $('#cctv').show();
         $('#security').show();
         $('#furnished').show();
         $('#pets').show();
         $('#electricfence').show();
         $('#backupgenerator').show();
         $('#parking').show();
         $('#liftelevator').show();
         $('#negotiable').show();
       }
       else if(propertyTypeId == 6 || propertyTypeId == 7 || propertyTypeId == 4) { //1,2,3 Bedroom
         $('#extraOptions').show();
         $('#parking').show();
         $('#hasBalcony').show();
         $('#gated').show();
         $('#cctv').show();
         $('#security').show();
         $('#swimmingpool').hide();
         $('#playground').hide();
         $('#furnished').hide();
         $('#pets').hide();
         $('#electricfence').hide();
         $('#backupgenerator').hide();
         $('#liftelevator').hide();
         $('#negotiable').show();
       }
       else {
         $('#extraOptions').hide();
         $('#gated').hide();
         $('#hasBalcony').hide();
         $('#swimmingpool').hide();
         $('#playground').hide();
         $('#cctv').hide();
         $('#security').hide();
         $('#furnished').hide();
         $('#pets').hide();
         $('#electricfence').hide();
         $('#backupgenerator').hide();
         $('#parking').hide();
         $('#liftelevator').hide();
         $('#negotiable').hide();
       }
   });
</script>

</body>

</html>
