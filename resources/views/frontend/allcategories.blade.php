<section class="section">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- Section title -->
				<div class="section-title">
					<h2>All Categories</h2>
					<p>Select your preferred category & browse through!</p>
				</div>
				<div class="row">
					<!-- Category list col-lg-3 col-md-7 offset-md-1 offset-lg-0-->

          <?php
          $categoriestop = App\Propertycategories::getAllAll();
          foreach ($categoriestop as $keycat) {
          ?>
					<div class="col-lg-3 col-md-6 offset-md-1 offset-lg-0">
						<div class="category-block">
							<div class="header">
								<i class="<?php echo $keycat->icon; ?>"></i>
								<h4><a href="<?php $url = URL::to("/viewByCategory"."/".$keycat->id); print_r($url); ?>">
                  <?php echo $keycat->categoryName; ?></a></h4>
							</div>
							<ul class="category-list" >
                <?php
                $propertytypes = App\Propertytypes::getByCategoryIdAll($keycat->id);
                foreach ($propertytypes as $keyprop) {
                ?>
								<li><a href="<?php $url = URL::to("/viewByType"."/".$keycat->id); print_r($url); ?>"><?php echo $keyprop->typeName; ?> <span><?php echo $keyprop->count; ?></span></a></li>
                <?php
                }
                ?>
							</ul>
						</div>
					</div> <!-- /Category List -->
          <?php
          }
          ?>


				</div>
			</div>
		</div>
	</div>
	<!-- Container End -->
</section>
