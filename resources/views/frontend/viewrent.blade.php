<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Rent Poa</title>

  @include('frontend.headerlinks')

<link rel='stylesheet' href="{{ URL::asset('gallery/css/slick.css')}}">
<link rel='stylesheet' href="{{ URL::asset('gallery/css/jquery.fancybox.min.css')}}">

<style>
.slick-slider .slick-prev, .slick-slider .slick-next {
  z-index: 100;
  font-size: 2.5em;
  height: 40px;
  width: 40px;
  margin-top: -20px;
  /* color: #B7B7B7; */
  position: absolute;
  top: 50%;
  text-align: center;
  color: #ffffff;
  /* opacity: .3; */
  transition: opacity .25s;
  cursor: pointer;
	background: #5672f9;
  line-height: 30px !important;
}
.slick-slider .slick-prev:hover, .slick-slider .slick-next:hover {
  opacity: .65;
}
.slick-slider .slick-prev {
  left: 0;
}
.slick-slider .slick-next {
  right: 0;
}

#detail .product-images {
  width: 100%;
  margin: 0 auto;
  border:1px solid #eee;
}
#detail .product-images li, #detail .product-images figure, #detail .product-images a, #detail .product-images img {
  display: block;
  outline: none;
  border: none;
}
#detail .product-images .main-img-slider figure {
  margin: 0 auto;
  padding: 0 2em;
}
#detail .product-images .main-img-slider figure a {
  cursor: pointer;
  cursor: -webkit-zoom-in;
  cursor: -moz-zoom-in;
  cursor: zoom-in;
}
#detail .product-images .main-img-slider figure a img {
  width: 100%;
  max-width: 400px;
  margin: 0 auto;
}
#detail .product-images .thumb-nav {
  margin: 0 auto;
  padding:20px 10px;
  max-width: 600px;
}
#detail .product-images .thumb-nav.slick-slider .slick-prev, #detail .product-images .thumb-nav.slick-slider .slick-next {
  font-size: 1.2em;
  height: 20px;
  width: 26px;
  margin-top: -10px;
}
#detail .product-images .thumb-nav.slick-slider .slick-prev {
  margin-left: -30px;
}
#detail .product-images .thumb-nav.slick-slider .slick-next {
  margin-right: -30px;
}
#detail .product-images .thumb-nav li {
  display: block;
  margin: 0 auto;
  cursor: pointer;
}
#detail .product-images .thumb-nav li img {
  display: block;
  width: 100%;
  max-width: 75px;
  margin: 0 auto;
  border: 2px solid transparent;
  -webkit-transition: border-color .25s;
  -ms-transition: border-color .25s;
  -moz-transition: border-color .25s;
  transition: border-color .25s;
}
#detail .product-images .thumb-nav li:hover, #detail .product-images .thumb-nav li:focus {
  border-color: #999;
}
#detail .product-images .thumb-nav li.slick-current img {
  border-color: #d12f81;
}

.main_img { width: 500px; margin: 0 auto}

/* The Modal (background) */
.modalcontactOwner {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content   background-color: #fefefe;*/
.modal-content-contactOwner {
  margin: auto;
    background-color: #FFFFFF;
  -moz-border-radius: 10px;
  -webkit-border-radius: 10px;
  border-radius: 10px;
  -moz-box-shadow: .3em .3em .5em rgba(0, 0, 0, .8);
  -webkit-box-shadow: .3em .3em .5em rgba(0, 0, 0, .8);
  box-shadow: .3em .3em .5em rgba(0, 0, 0, .8);
  padding: 20px;
  width: 30%;
}

.contentsuccess {
  border: 2px solid #FFD700;
}

/* The Close Button */
.close-modal-contactOwner {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close-modal-contactOwner:hover,
.close-modal-contactOwner:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>

</head>

<body class="body-wrapper">

@include('frontend.top')

<!--===============================
=            Hero Area            =
================================-->

<section class="blog single-blog section">
	<div class="container">
		<div class="row">
			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<article class="single-post">
					<h2><?php echo $propertyDetails->title; ?></h2>
					<ul class="list-inline">
						<li class="list-inline-item">by <a href=""><?php echo $propertyDetails->firstName; ?> <?php echo $propertyDetails->lastName; ?></a></li>
						<li class="list-inline-item"><?php echo date('jS F Y H:i', strtotime($propertyDetails->created_at)); ?></li>
					</ul>

<button class="btnlabel btn-warning" onclick="ContactOwner(<?php echo $propertyDetails->id; ?>)">Ksh. <?php echo number_format($propertyDetails->amount,0); ?></button>
<button class="btn btn-primary" id="contactOwner" onclick="ContactOwner(<?php echo $propertyDetails->id; ?>)">Contact Owner</button>

          <!-- ////////////////////////////////////////////////////////////// -->
          <section id="detail">
<div class="container">
<div class="row">
<div class="main_img">

<div class="product-images demo-gallery">

<div class="main-img-slider">
  <?php foreach ($propertyPhotos as $keypp) { ?>
<a data-fancybox="gallery" href="{{ URL::to('/') }}/allphotos/<?php echo $keypp->photo; ?>"><img src="{{ URL::to('/') }}/allphotos/<?php echo $keypp->photo; ?>" class="img-fluid"  alt="RentPoa - <?php echo $propertyDetails->title; ?>" title="RentPoa - <?php echo $propertyDetails->title; ?>"></a>
<?php } ?>

</div>


<ul class="thumb-nav">
<?php foreach ($propertyPhotos as $keyppi) { ?>
<li><img src="{{ URL::to('/') }}/allphotos/<?php echo $keyppi->photo; ?>" alt="RentPoa - <?php echo $propertyDetails->title; ?>" title="RentPoa - <?php echo $propertyDetails->title; ?>"></li>
<?php } ?>

</ul>

</div>

</div>
</div>
</div>
</section>
          <!-- ////////////////////////////////////////////////////////////// -->

<hr>
<p>
<h3>Description</h3> <?php echo $propertyDetails->description; ?>
</p>
<p>
<h3>Directions</h3> <?php echo $propertyDetails->directions; ?>
</p>

<style>
.pdetailsbox {
  width: 25%;
}
</style>

<h3>Available Facilities & Amenities</h3>
<div class="row" style="margin-left:1%;">
  <?php
  if ($propertyDetails->parking == 1) {
    echo "<div class=pdetailsbox>Has Parking</div>";
  }
  if ($propertyDetails->hasBalcony == 1) {
    echo "<div class=pdetailsbox>Has Balcon</div>";
  }
  // if ($propertyDetails->managementTypeId == 1) {
  // 	$managementTypeId = 0;
  // }
  // $depositTypeId = 0;
  if ($propertyDetails->swimmingpool == 1) {
    echo "<div class=pdetailsbox>Has Swimming Pool</div>";
  }
  if ($propertyDetails->playground == 1) {
    echo "<div class=pdetailsbox>Has Playground</div>";
  }
  if ($propertyDetails->gated == 1) {
    echo "<div class=pdetailsbox>Is Gated</div>";
  }
  if ($propertyDetails->security == 1) {
    echo "<div class=pdetailsbox>Has Security Guards</div>";
  }
  if ($propertyDetails->cctv == 1) {
    echo "<div class=pdetailsbox>Has CCTV</div>";
  }
  if ($propertyDetails->furnished == 1) {
    echo "<div class=pdetailsbox>Is Furnished</div>";
  }
  if ($propertyDetails->negotiable == 1) {
    echo "<div class=pdetailsbox>Negotiable Rent</div>";
  }
  if ($propertyDetails->backupgenerator == 1) {
    echo "<div class=pdetailsbox>Has Backup Generator</div>";
  }
  if ($propertyDetails->liftelevator == 1) {
    echo "<div class=pdetailsbox>Has Lift Elevator</div>";
  }
  if ($propertyDetails->electricfence == 1) {
    echo "<div class=pdetailsbox>Has Electric Fence</div>";
  }
  if ($propertyDetails->pets == 1) {
    echo "<div class=pdetailsbox>Pets Allowed</div>";
  }
  ?>
</div>

<?php
	 $title=urlencode($propertyDetails->title);
	 $url=urlencode(url()->full());
   $allphotos = App\Propertyphotos::where('propertyId',$propertyDetails->id)->where('is_deleted',0)->first();
	 $image=urlencode(url('/')."allphotos/".$allphotos->photo);
?>

          <ul class="social-circle-icons list-inline" style="margin-top:3%;">
				  		<li class="list-inline-item text-center"><a class="fa fa-facebook" onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title;?>&amp;p[url]=<?php echo $url; ?>&amp;&p[images][0]=<?php echo $image;?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)"></a></li>
				  		<li class="list-inline-item text-center"><a class="fa fa-twitter" target="_blank" href="http://twitter.com/home?status=<?php echo $title; echo " "; echo url()->full(); ?>"></a></li>
				  		<li class="list-inline-item text-center"><a class="fa fa-whatsapp" target="_blank" href="https://web.whatsapp.com/send?text=https%3A//jiji.co.ke/membley-estate/houses-apartments-for-rent/to-let-bedsitter-1-2-and-3-bedroom-houses-to-let-in-membley-estate-hy4gaXtCyE5xKd7aSxoe1rps.html%3Futm_campaign%3DwhatsappButton%26utm_medium%3Dsite-share-button%26utm_source%3Dwhatsapp"></a></li>
				  </ul>

          <button class="btnlabel btn-warning" onclick="ContactOwner(<?php echo $propertyDetails->id; ?>)">Ksh. <?php echo number_format($propertyDetails->amount,0); ?></button>
          <button class="btn btn-primary" id="contactOwner" onclick="ContactOwner(<?php echo $propertyDetails->id; ?>)">Contact Owner</button>

          <!-- Success Modal -->
          <div id="ContactOwnerModal" class="modalcontactOwner">
            <div class="modal-content-contactOwner contentsuccess">
              <span id="close-success-modal-contactOwner" class="close-modal-contactOwner">&times;</span>

              <center>
              <?php if($propertyDetails->isVerified == 1) { ?>
                 <img src="{{ URL::asset('images/verified_by_rent_poa.jpg')}}" style="width:30%;"/>
              <?php } else { ?>
                 <img src="{{ URL::asset('images/not_verified_by_rent_poa.png')}}" style="width:30%;"/>
              <?php } ?>

              <div class="row">
  							<div class="col-sm-12 col-md-12">
                  <h2><?php echo $propertyDetails->firstName; ?> <?php echo $propertyDetails->lastName; ?></h2>
                  <h2>+<?php echo $propertyDetails->mobileNo; ?></h2>
  							</div>
              </div>

              <div class="col-sm-12 col-md-8">
              <button class="btn btn-primary" id="closeModal" onclick="closeModal()">Close</button>
              </div>
              </center>

            </div>
          </div>


				</article>
				<!-- <div class="block comment">
					<h4>Leave A Comment</h4>
					<form action="#">
						<div class="form-group mb-30">
						    <label for="message">Message</label>
						    <textarea class="form-control" id="message" rows="8"></textarea>
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-6">
								<div class="form-group mb-30">
								    <label for="name">Name</label>
								    <input type="text" class="form-control" id="name">
								</div>
							</div>
							<div class="col-sm-12 col-md-6">
								<div class="form-group mb-30">
								    <label for="email">Email</label>
								    <input type="email" class="form-control" id="email">
								</div>
							</div>
						</div>
						<button class="btn btn-transparent">Leave Comment</button>
					</form>
				</div> -->
			</div>

      @include('frontend.rightbar')

		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ URL::asset('gallery/js/popper.min.js')}}"></script>
<script src="{{ URL::asset('gallery/js/slick.min.js')}}"></script>
<script src="{{ URL::asset('gallery/js/jquery.fancybox.min.js')}}"></script>
<script id="rendered-js">
/*--------------*/



// Main/Product image slider for product page
$('#detail .main-img-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  arrows: true,
  fade: true,
  autoplay: true,
  autoplaySpeed: 4000,
  speed: 300,
  lazyLoad: 'ondemand',
  asNavFor: '.thumb-nav',
  prevArrow: '<div class="slick-prev"><i class="i-prev"></i><span class="sr-only sr-only-focusable"><</span></div>',
  nextArrow: '<div class="slick-next"><i class="i-next"></i><span class="sr-only sr-only-focusable">></span></div>' });

// Thumbnail/alternates slider for product page
$('.thumb-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  infinite: true,
  centerPadding: '0px',
  asNavFor: '.main-img-slider',
  dots: false,
  centerMode: false,
  draggable: true,
  speed: 200,
  focusOnSelect: true,
  prevArrow: '<div class="slick-prev"><i class="i-prev"></i><span class="sr-only sr-only-focusable"><</span></div>',
  nextArrow: '<div class="slick-next"><i class="i-next"></i><span class="sr-only sr-only-focusable">></span></div>' });



//keeps thumbnails active when changing main image, via mouse/touch drag/swipe
$('.main-img-slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
  //remove all active class
  $('.thumb-nav .slick-slide').removeClass('slick-current');
  //set active class for current slide
  $('.thumb-nav .slick-slide:not(.slick-cloned)').eq(currentSlide).addClass('slick-current');
});
//# sourceURL=pen.js
</script>

<script>
//function ContactOwner(propertyId) {
  // Get the modal
var ContactOwnerModal = document.getElementById("ContactOwnerModal");
// Get the <spanmodalclose> element that closes the modal
var spanContactOwnermodalclose = document.getElementById("close-success-modal-contactOwner");
var closeModal = document.getElementById("closeModal");
// When the user clicks the button, open the modal
function ContactOwner(propertyId) {
  ContactOwnerModal.style.display = "block";
}
// When the user clicks on <spanmodalclose> (x), close the modal
spanContactOwnermodalclose.onclick = function() {
  ContactOwnerModal.style.display = "none";
}
closeModal.onclick = function() {
  ContactOwnerModal.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == ContactOwnerModal) {
    //ContactOwnerModal.style.display = "none";
  }
}
//}


// function SubmitPhone(propertyId) {
//   var mobileNo = document.getElementById("mobileNo");
//   console.log("mobileNo---"+mobileNo.value);
//   if(mobileNo.value.length != 10) {
//     alert("Please enter valid phone number");
//   }
//   else {
//
//     $('#loadingmessage').show();
//       $.ajax({
//       type : 'get',
//       url : '{{URL::to('submitPhone')}}',
//       data:{'mobileNo':mobileNo.value},
//       success:function(data){
//         $('#loadingmessage').hide();
//
//         if (data == 200){
//
//           console.log("completed success");
//           $('#mobileNo').hide();
//           $('#phoneVerification').hide();
//           $('#SubmitPhone').hide();
//           document.getElementById("showSMSdescription").innerHTML = "An SMS verification code has been sent to ["+mobileNo+"]. Please enter it in the form below";
//           document.getElementById("codeVerification").style.display = "block";
//           document.getElementById("smsCode").style.display = "block";
//           document.getElementById("verifyCode").style.display = "block";
//
//         }
//         else {
//           alert("Failed to send SMS to your phone number. Check the correctness of the number and try again.");
//         }
//       }
//       });
//
//   }
// }
</script>
</body>

</html>
