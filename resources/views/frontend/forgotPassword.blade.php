<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Forgot Password</title>

@include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.top')

<!--==================================
=            User Profile            =
===================================-->

<section class="user-profile section">
	<div class="container">
		<div class="row">

			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Edit Personal Info -->
				<div class="widget personal-info">
          <h2>Recover Password</h2>
					<h3 class="widget-header user">Enter your registered email address. We shall send recovery instructions to this email</h3>

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
          @endif

          @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
          @endif

            {!! Form::open(['url'=>'resetPassword']) !!}
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
						    <label for="comunity-name">Email Address</label>
						    <input type="email" class="form-control" name="email" id="email"  value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
						</div>

						<div class="form-check">
						     <a href="{{URL::to('/login')}}" class="linkref">Back to login</a>
						</div>

						<!-- Submit button -->
						<button class="btn btn-main" type="submit">Recover</button>
					{!! Form::close() !!}
				</div>

			</div>

      @include('frontend.rightbar')

		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

</body>

</html>
