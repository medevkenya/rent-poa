<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>My Rental</title>

  @include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.topinner')

<!--==================================
=            User Profile            =
===================================-->

<section class="user-profile section">
	<div class="container">
		<div class="row">

			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Edit Personal Info -->
				<div class="widget personal-info">
          <h2>My Rental</h2>
          <ul class="list-inline">
						<li class="list-inline-item">Category <a href=""><?php echo $propertyDetails->propertytypeName; ?></a></li>
						<li class="list-inline-item"><?php echo $propertyDetails->created_at; ?></li>
					</ul>

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
          @endif

          <?php
          if($propertyDetails->status == 0) {
            $status = "Inactive";
          }
          else {
            $status = "Active";
          }
          ?>

          <h3 class="title"><?php echo $propertyDetails->title; ?></h3>
          <p><span class="add-id"><strong>Rental ID:</strong> <?php echo $propertyDetails->pid; ?></span></p>
          <p><span><strong>Description: </strong><time> <?php echo $propertyDetails->description; ?></time> </span></p>
          <p><span><strong>Directions: </strong><time> <?php echo $propertyDetails->directions; ?></time> </span></p>
          <p><span class="status active"><strong>Status:</strong> <?php echo $status; ?></span></p>
          <p><span class="location"><strong>Views:</strong> (<?php echo $propertyDetails->views; ?>)</span></p>

          <div class="row">
            <div class="col-md-12">
          <?php
          if ($propertyDetails->parking == 1) {
    				echo "<div class=form-group col-md-4>Has Parking</div>";
    			}
    			if ($propertyDetails->hasBalcony == 1) {
    				echo "<div class=form-group col-md-4>Has Balcon</div>";
    			}
    			// if ($propertyDetails->managementTypeId == 1) {
    			// 	$managementTypeId = 0;
    			// }
    			// $depositTypeId = 0;
    			if ($propertyDetails->swimmingpool == 1) {
    				echo "<div class=form-group col-md-4>Has Swimming Pool</div>";
    			}
    			if ($propertyDetails->playground == 1) {
    				echo "<div class=form-group col-md-4>Has Playground</div>";
    			}
    			if ($propertyDetails->gated == 1) {
    				echo "<div class=form-group col-md-4>Is Gated</div>";
    			}
    			if ($propertyDetails->security == 1) {
    				echo "<div class=form-group col-md-4>Has Security Guards</div>";
    			}
    			if ($propertyDetails->cctv == 1) {
    				echo "<div class=form-group col-md-4>Has CCTV</div>";
    			}
    			if ($propertyDetails->furnished == 1) {
    				echo "<div class=form-group col-md-4>Is Furnished</div>";
    			}
    			if ($propertyDetails->negotiable == 1) {
    				echo "<div class=form-group col-md-4>Negotiable Rent</div>";
    			}
    			if ($propertyDetails->backupgenerator == 1) {
    				echo "<div class=form-group col-md-4>Has Backup Generator</div>";
    			}
    			if ($propertyDetails->liftelevator == 1) {
    				echo "<div class=form-group col-md-4>Has Lift Elevator</div>";
    			}
    			if ($propertyDetails->electricfence == 1) {
    				echo "<div class=form-group col-md-4>Has Electric Fence</div>";
    			}
    			if ($propertyDetails->pets == 1) {
    				echo "<div class=form-group col-md-4>Pets Allowed</div>";
    			}
          ?>
        </div>
        </div>

            <div class="row">
              <h3 class="title">Photos (At least 4, Maximum 9)</h3>
            <?php
            $photos = App\Propertyphotos::where('propertyId',$propertyDetails->id)->where('is_deleted',0)->orderBy('id','DESC')->get();
            foreach ($photos as $keyphoto) {
            ?>
            <div class="form-group imgcontainer col-md-4">
              <img class="imagepreview" style="height:200px;width:200px;" src="{{ URL::to('/') }}/allphotos/<?php echo $keyphoto->photo; ?>" alt="RentPoa - <?php echo $propertyDetails->title; ?>">
              <a href="<?php $url = URL::to("/removePhoto"."/".$keyphoto->id); print_r($url); ?>" onclick="return confirm('Are you sure you want to remove this photo?');">
               <button class="btn">Remove</button>
             </a>
            </div>
            <?php
            }
             ?>
             <form action="{{ route('addPhoto') }}" method="post" enctype="multipart/form-data">
             {{ csrf_field() }}
             <div class="row">
               <input type="hidden" name="propertyId" value="<?php echo $propertyDetails->id; ?>">
             <div class="form-group  col-md-12">
 						    <label for="first-name">Browse for a photo</label>
 						    <input type="file" class="form-control" name="photo" id="photo" required>
                 @if ($errors->has('photo'))
                     <span class="text-danger">{{ $errors->first('photo') }}</span>
                 @endif
 						</div>

           </div>
 						<!-- Submit button -->
 						<button class="btn btn-main" type="submit">Upload</button>
 					{!! Form::close() !!}
           </div>
          </div>

				</div>


      @include('frontend.rightbarinner')

		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

@include('frontend.footerlinks')

</body>

</html>
