<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>My Picture</title>

  @include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.topinner')

<!--==================================
=            User Profile            =
===================================-->

<section class="user-profile section">
	<div class="container">
		<div class="row">

			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Edit Personal Info -->
				<div class="widget personal-info">
          <h2>Update Profile Picture</h2>

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
          @endif

            <form action="{{ route('editPic') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">

            <div class="form-group  col-md-12">
						    <label for="first-name">Browse for a photo</label>
						    <input type="file" class="form-control" name="profilepic" id="profilepic" required>
                @if ($errors->has('profilepic'))
                    <span class="text-danger">{{ $errors->first('profilepic') }}</span>
                @endif
						</div>

          </div>
						<!-- Submit button -->
						<button class="btn btn-main" type="submit">Upload</button>
					{!! Form::close() !!}
				</div>

			</div>

      @include('frontend.rightbarinner')

		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

@include('frontend.footerlinks')

</body>

</html>
