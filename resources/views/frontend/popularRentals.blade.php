<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Popular Rentals | Rent Poa</title>

  @include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.top')

<!--===============================
=            Hero Area            =
================================-->

<section class="hero-area bg-1 text-center overly">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Header Contetnt -->
				<div class="content-block">
					<h1>Popular Rentals Near You </h1>
					<div class="short-popular-category-list text-center">
						<!-- <h2>Popular Category</h2> -->
						<ul class="list-inline">
              <?php
              $subcountieslist = App\Subcounties::getAll();
              foreach ($subcountieslist as $keysub) {
              ?>
							<li class="list-inline-item">
								<a href="<?php $url = URL::to("/viewByLocation"."/".$keysub->id); print_r($url); ?>"><i class="fa fa-map-marker"></i> <?php echo $keysub->subCountyName; ?> (<?php echo $keysub->count; ?>)</a></li>
              <?php
              }
              ?>
						</ul>
					</div>

				</div>
				<!-- Advance Search -->
				<div class="advance-search">
					<form action="#">
						<div class="row">
							<!-- Store Search -->
							<div class="col-lg-2 col-md-12">
								<div class="block d-flex">
									<select class="form-control mb-2 mr-sm-2 mb-sm-0" id="search">
                    <option>Category</option>
                    <option>Houses for Rent</option>
                    <option>Shops for Rent</option>
                    <option>Offices for Rent</option>
                    <option>Venue for Rent</option>
                  </select>
								</div>
							</div>
              <div class="col-lg-2 col-md-12">
								<div class="block d-flex">
									<select class="form-control mb-2 mr-sm-2 mb-sm-0" id="search">
                    <option>Type</option>
                    <option>1 Bedroom</option>
                    <option>Bedsitter</option>
                    <option>3 Bedroom</option>
                    <option>Single</option>
                  </select>
								</div>
							</div>
              <div class="col-lg-3 col-md-12">
								<div class="block d-flex">
									<select class="form-control mb-2 mr-sm-2 mb-sm-0" id="search">
                    <option>Preferred Location</option>
                    <option>Nairobi</option>
                    <option>Kisumu</option>
                    <option>Nakuru</option>
                    <option>Eldoret</option>
                  </select>
								</div>
							</div>
              <div class="col-lg-3 col-md-12">
								<div class="block d-flex">
									<select class="form-control mb-2 mr-sm-2 mb-sm-0" id="search">
                    <option>Approximate Cost</option>
                    <option>3000</option>
                    <option>5000</option>
                    <option>10000</option>
                    <option>15000</option>
                  </select>
								</div>
							</div>
							<div class="col-lg-2 col-md-12">
								<div class="block d-flex">
									<button class="btn btn-main btn-block">SEARCH</button>
								</div>
							</div>
						</div>
					</form>

				</div>

			</div>
		</div>
	</div>
	<!-- Container End -->
</section>

<!--===================================
=            Client Slider            =
====================================-->


<!--===========================================
=            Popular deals section            =
============================================-->

<section class="popular-deals section bg-gray stagesection">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<h2>Trending Properties</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- offer 01 -->

      <?php foreach ($data as $key) {

        $photoDetails = App\Propertyphotos::where('propertyId',$key->id)->orderBy('id','DESC')->limit(1)->first();
        if($photoDetails) {
        $featuredPhoto = $photoDetails->photo;
        }
        else {
          $featuredPhoto = "rental.jpg";
        }
        ?>
			<div class="col-sm-12 col-lg-3">
				<!-- product card -->
<div class="product-item bg-light">
	<div class="card">
		<div class="thumb-content rentimgcontainer">
			<!-- <div class="price">$200</div> -->
			<a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>">
				<img class="card-img-top img-fluid" src="{{ URL::to('/') }}/allphotos/<?php echo $featuredPhoto; ?>" alt="RentPoa - <?php echo $key->title; ?>">
        <!-- <button class="btn">RentPoa</button> -->
      </a>
		</div>
		<div class="card-body">
      <div class="item-title">
		    <h4 class="card-title"><a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>"><?php echo $key->title; ?></a></h4>
      </div>
        <ul class="list-inline product-meta">
		    	<li class="list-inline-item">
		    		<a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>"><i class="fa fa-folder-open-o"></i><?php echo $key->propertytypeName; ?></a>
		    	</li>
		    	<li class="list-inline-item">
		    		<a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>"><i class="fa fa-calendar"></i><?php echo date('jS F', strtotime($key->created_at)); ?></a>
		    	</li>
		    </ul>
		    <p class="card-text"><a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>"><i class="fa fa-map-marker"></i> <?php echo $key->subCountyName; ?> <span style="color:#FFD700;font-weight:600;">(Views <?php echo $key->views; ?>)</span></a></p>
        <p class="card-text"><a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>"><i class="fa fa-user"></i> <?php echo $key->firstName; ?> <?php echo $key->lastName; ?></a></p>
		    <div class="product-ratings">
		    	<a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>" class="btnbig btn-primary btn-block">Ksh. <?php echo number_format($key->amount,0); ?></a>
		    </div>
		</div>
	</div>
</div>
</div>

<?php } ?>



		</div>
	</div>
</section>



<!--==========================================
=            All Category Section            =
===========================================-->

@include('frontend.allcategories')

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

</body>

</html>
