<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-expand-lg  navigation">
					<a class="navbar-brand" href="{{URL::to('/dashboard')}}">
						<img src="{{ URL::asset('images/logo.png')}}" style="width:40%;" alt="RentPoa">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto main-nav ">
							<li class="nav-item active">
								<a class="nav-link" href="{{URL::to('/dashboard')}}">My Rentals</a>
							</li>
							<li class="nav-item">
								<a class="nav-link add-button" href="{{URL::to('/addListing')}}"><i class="fa fa-plus-circle"></i> Add Rental </a>
							</li>
						</ul>
						<ul class="navbar-nav ml-auto mt-10">
							<li class="nav-item">
								<a class="nav-link login-button" href="{{URL::to('/profile')}}">My Profile</a>
							</li>
							<li class="nav-item">
								<a class="nav-link login-button" href="{{URL::to('/changepassword')}}">Change Password</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</section>
