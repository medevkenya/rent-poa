<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-expand-lg  navigation">
					<a class="navbar-brand" href="{{URL::to('/')}}">
						<img src="{{ URL::asset('images/logo.png')}}" class="rentpoa-logo" style="width:40%;" alt="RentPoa">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto main-nav ">
							<li class="nav-item active">
								<a class="nav-link" href="{{URL::to('/')}}">Home</a>
							</li>

							<li class="nav-item dropdown dropdown-slide">
								<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Categories <span><i class="fa fa-angle-down"></i></span>
								</a>
								<!-- Dropdown list -->
								<div class="dropdown-menu dropdown-menu-right">
									<?php
                  $categoriestop = App\Propertycategories::getAllAll();
                  foreach ($categoriestop as $keycat) {
                  ?>
									<a class="dropdown-item" href="<?php $url = URL::to("/viewByCategory"."/".$keycat->id); print_r($url); ?>"><?php echo $keycat->categoryName; ?></a>
									<?php
									}
                  ?>
								</div>
							</li>
							<li class="nav-item">
								<a class="nav-link add-button" href="{{URL::to('/addrental')}}"><i class="fa fa-plus-circle"></i> Add Rental </a>
							</li>
						</ul>
						<ul class="navbar-nav ml-auto mt-10">
							<li class="nav-item">
								<a class="nav-link login-button" href="{{URL::to('/login')}}">Login</a>
							</li>
							<li class="nav-item">
								<a class="nav-link login-button" href="{{URL::to('/register')}}">Create Account</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</section>
