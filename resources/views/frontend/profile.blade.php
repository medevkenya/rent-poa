<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Profile</title>

  @include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.topinner')

<!--==================================
=            User Profile            =
===================================-->

<section class="user-profile section">
	<div class="container">
		<div class="row">

			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Edit Personal Info -->
				<div class="widget personal-info">
          <h2>Update Profile</h2>

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
          @endif

            {!! Form::open(['url'=>'updateProfile']) !!}
            {{ csrf_field() }}
            <div class="row">

            <div class="form-group  col-md-12">
						    <label for="first-name">First Name</label>
						    <input type="text" class="form-control" name="firstName" id="firstName"  value="<?php echo $details->firstName; ?>" required>
                @if ($errors->has('firstName'))
                    <span class="text-danger">{{ $errors->first('firstName') }}</span>
                @endif
						</div>

            <div class="form-group  col-md-12">
						    <label for="first-name">Last Name</label>
						    <input type="text" class="form-control" name="lastName" id="lastName"  value="<?php echo $details->lastName; ?>" required>
                @if ($errors->has('lastName'))
                    <span class="text-danger">{{ $errors->first('lastName') }}</span>
                @endif
						</div>

            <div class="form-group  col-md-12">
						    <label for="first-name">Mobile Number</label>
						    <input type="text" class="form-control" name="mobileNo" id="mobileNo"  value="<?php echo $details->mobileNo; ?>" required>
                @if ($errors->has('mobileNo'))
                    <span class="text-danger">{{ $errors->first('mobileNo') }}</span>
                @endif
						</div>

          </div>
						<!-- Submit button -->
						<button class="btn btn-main" type="submit">Save Changes</button>
					{!! Form::close() !!}
				</div>

			</div>

      @include('frontend.rightbarinner')

		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

@include('frontend.footerlinks')

</body>

</html>
