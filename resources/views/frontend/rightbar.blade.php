<div class="col-md-10 offset-md-1 col-lg-4 offset-lg-0">
  <div class="sidebar">

    <!-- Category Widget -->
    					<div class="widget category">
    						<!-- Widget Header -->
    						<h5 class="widget-header">Categories</h5>
    						<ul class="category-list">
                  <?php
                  $categoriestop = App\Propertycategories::getAllAll();
                  foreach ($categoriestop as $keycat) {
                  ?>
    							<li><a href="<?php $url = URL::to("/viewByCategory"."/".$keycat->id); print_r($url); ?>"><?php echo $keycat->categoryName; ?> <span class="float-right">(<?php echo $keycat->count; ?>)</span></a></li>
                  <?php
                  }
                  ?>
    						</ul>
    					</div>

              <!-- Locations Widget -->
            <div class="widget category">
              <!-- Widget Header -->
              <h5 class="widget-header">Locations</h5>
              <ul class="category-list">
                <?php
                $subcountieslist = App\Subcounties::getAll();
                foreach ($subcountieslist as $keysub) {
                ?>
                <li><a href="<?php $url = URL::to("/viewByLocation"."/".$keysub->id); print_r($url); ?>"><?php echo $keysub->subCountyName; ?> <span class="float-right">(<?php echo $keysub->count; ?>)</span></a></li>
                <?php
                }
                ?>
              </ul>
            </div>

              <!-- Rate Widget -->
<!-- <div class="widget rate">
  <h5 class="widget-header text-center">What would you rate
  <br>
  this product</h5>
  <div class="starrr"></div>
</div> -->
<!-- Safety tips widget -->
<!-- <div class="widget disclaimer">
  <h5 class="widget-header">Safety Tips</h5>
  <ul>
    <li>Meet seller at a public place</li>
    <li>Check the item before you buy</li>
    <li>Pay only after collecting the item</li>
    <li>Pay only after collecting the item</li>
  </ul>
</div> -->
<!-- Coupon Widget -->
<div class="widget coupon text-center">
  <!-- Coupon description -->
  <p>Have a great rental to post ? Share it with us.
  </p>
  <!-- Submii button -->
  <a href="{{URL::to('/addrental')}}" class="btn btn-transparent-white">Submit Listing</a>
</div>

  </div>
</div>
