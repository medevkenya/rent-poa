<div class="col-md-10 offset-md-1 col-lg-4 offset-lg-0">
  <div class="sidebar">
    <!-- User Widget -->
    <div class="widget user-dashboard-profile">
      <!-- User Image -->
      <div class="profile-thumb">
        <img src="{{ URL::to('/') }}/profiles/{{{ Auth::user()->profilepic }}}" alt="RentPoa" class="rounded-circle">
      </div>
      <!-- User Name -->
      <h5 class="text-center">{{{ Auth::user()->firstName }}} {{{ Auth::user()->lastName }}}</h5>
      <p>{{{ Auth::user()->email }}}</p>
      <p>{{{ Auth::user()->mobileNo }}}</p>
      <a href="{{URL::to('/mypic')}}" class="btn btn-main-sm">Edit Picture</a>
    </div>
    <!-- Dashboard Links -->
    <div class="widget user-dashboard-menu">
      <ul>
        <li class="active">
          <a href="{{URL::to('/addListing')}}"><i class="fa fa-user"></i> My Ads</a>
        </li>
        <!-- <li>
          <a href="dashboard-favourite-ads.html"><i class="fa fa-bookmark-o"></i> Favourite Ads <span>5</span></a>
        </li>
        <li>
          <a href="dashboard-archived-ads.html"><i class="fa fa-file-archive-o"></i>Archeved Ads <span>12</span></a>
        </li>
        <li>
          <a href="dashboard-pending-ads.html"><i class="fa fa-bolt"></i> Pending Approval<span>23</span></a>
        </li> -->
        <li>
          <a href="{{URL::to('/logout')}}"><i class="fa fa-cog"></i> Logout</a>
        </li>
        <!-- <li>
          <a href="{{URL::to('/notifications')}}"><i class="fa fa-power-off"></i>Delete Account</a>
        </li> -->
      </ul>
    </div>
  </div>
</div>
