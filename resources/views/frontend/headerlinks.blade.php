<link rel="preconnect" href="//www.google.com/">
<link rel="preconnect" href="//www.googleadservices.com/">
<link rel="preconnect" href="//www.googletagservices.com/">
<link rel="preconnect" href="//www.googletagmanager.com/">
<link rel="preconnect" href="//www.google-analytics.com/">

<!-- PLUGINS CSS STYLE -->
<link href="{{ URL::asset('assets/plugins/jquery-ui/jquery-ui.min.css')}}" rel="stylesheet">
<!-- Bootstrap -->
<link href="{{ URL::asset('assets/plugins/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{ URL::asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<!-- Owl Carousel -->
<!-- <link href="{{ URL::asset('assets/plugins/slick-carousel/slick/slick.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/plugins/slick-carousel/slick/slick-theme.css')}}" rel="stylesheet"> -->
<!-- Fancy Box -->
<!-- <link href="{{ URL::asset('assets/plugins/fancybox/jquery.fancybox.pack.css')}}" rel="stylesheet"> -->
<link href="{{ URL::asset('assets/plugins/jquery-nice-select/css/nice-select.css')}}" rel="stylesheet">
<!-- <link href="{{ URL::asset('assets/plugins/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css')}}" rel="stylesheet"> -->

<!-- CUSTOM CSS -->
<link href="{{ URL::asset('assets/css/style.css')}}" rel="stylesheet">

<!-- FAVICON -->
<link href="{{ URL::asset('assets/img/favicon.png')}}" rel="shortcut icon">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
