<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Terms of Use</title>

@include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.top')

<!--==================================
=            User Profile            =
===================================-->

<section class="user-profile section">
	<div class="container">
		<div class="row">

			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Edit Personal Info -->
				<div class="widget personal-info">
          <h2>Terms of use</h2>
					<h3 class="widget-header user">Your use of RentPoa is guided by the following terms & conditions.</h3>



				</div>

			</div>

      @include('frontend.rightbar')

		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

</body>

</html>
