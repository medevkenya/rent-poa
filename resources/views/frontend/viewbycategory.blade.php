<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Rent Poa</title>

  @include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.top')

<!--===============================
=            Hero Area            =
================================-->

<section class="hero-area bg-1 text-center overly">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Header Contetnt -->
				<div class="content-block">
					<h1><?php echo $categoryDetails->categoryName; ?></h1>
				</div>
				<!-- Advance Search -->
				<div class="advance-search">
          {!! Form::open(['url'=>'searchCategory']) !!}
          {{ csrf_field() }}
						<div class="row">
              <input type="hidden" name="categoryId" value="<?php echo $categoryDetails->id; ?>">
							<!-- Store Search -->
              <div class="col-lg-3 col-md-12">
								<div class="block d-flex">
                  <select class="form-control mb-2 mr-sm-2 mb-sm-0" id="propertytypes" name="propertyTypeId" required>
                    <option>Select Type</option>
                  </select>
								</div>
							</div>
              <div class="col-lg-3 col-md-12">
								<div class="block d-flex">
                  <select class="form-control mb-2 mr-sm-2 mb-sm-0" id="countyId" name="countyId" required>
                    <option>Select County</option>
                    <?php $counties = App\Counties::getAll();
                    foreach ($counties as $keyco) { ?>
                    <option value="<?php echo $keyco->id; ?>"><?php echo $keyco->countyName; ?></option>
                    <?php } ?>
                  </select>
								</div>
							</div>
              <div class="col-lg-2 col-md-12">
								<div class="block d-flex">
									<select class="form-control mb-2 mr-sm-2 mb-sm-0" name="subCountyId" id="subCountyId" required>
                    <option>Select Location</option>
                  </select>
								</div>
							</div>
              <div class="col-lg-2 col-md-12">
								<div class="block d-flex">
									<select class="form-control mb-2 mr-sm-2 mb-sm-0" id="approximateCost" name="approximateCost" required>
                    <option>Cost</option>
                    <option value="3000-5000">3,000 - 5,000</option>
                    <option value="6000-10000">6000 - 10000</option>
                    <option value="11000-15000">11000 - 15000</option>
                    <option value="16000-22000">16000 - 22000</option>
                    <option value="23000-30000">23000 - 30000</option>
                    <option value="31000-40000">31000 - 40000</option>
                    <option value="41000-55000">41000 - 55000</option>
                    <option value="56000-70000">56000 - 70000</option>
                    <option value="71000-85000">71000 - 85000</option>
                    <option value="86000-100000">86000 - 100000</option>
                    <option value=">100000">Above 100000</option>
                  </select>
								</div>
							</div>
							<div class="col-lg-2 col-md-12">
								<div class="block d-flex">
									<button class="btn btn-main btn-block" type="submit">SEARCH</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}

				</div>

			</div>
		</div>
	</div>
	<!-- Container End -->
</section>

<!--===================================
=            Client Slider            =
====================================-->


<!--===========================================
=            Popular deals section            =
============================================-->

<section class="popular-deals section bg-gray stagesection">
	<div class="container">

		<div class="row">
			<!-- offer 01 -->

      <?php if(!empty($data) && count($data) > 0) { ?>

        <?php if(isset($count)) { ?>
          <div class="col-md-12">
          <center>
            <p>Your search returned (<?php echo $count; ?>) rentals</p>
          </center>
        </div>
      <?php } ?>

      <?php foreach ($data as $key) {

        $photoDetails = App\Propertyphotos::where('propertyId',$key->id)->orderBy('id','DESC')->limit(1)->first();
        if($photoDetails) {
        $featuredPhoto = $photoDetails->photo;
        }
        else {
          $featuredPhoto = "rental.jpg";
        }
        ?>
			<div class="col-sm-12 col-lg-3">
				<!-- product card -->
<div class="product-item bg-light">
	<div class="card">
		<div class="thumb-content rentimgcontainer">
			<!-- <div class="price">$200</div> -->
			<a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>">
				<img class="card-img-top img-fluid" src="{{ URL::to('/') }}/allphotos/<?php echo $featuredPhoto; ?>" alt="RentPoa - <?php echo $key->title; ?>">
        <!-- <button class="btn">RentPoa</button> -->
      </a>
		</div>
		<div class="card-body">
      <div class="item-title">
		    <h4 class="card-title"><a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>"><?php echo $key->title; ?></a></h4>
      </div>
        <ul class="list-inline product-meta">
		    	<li class="list-inline-item">
		    		<a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>"><i class="fa fa-folder-open-o"></i><?php echo $key->propertytypeName; ?></a>
		    	</li>
		    	<li class="list-inline-item">
		    		<a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>"><i class="fa fa-calendar"></i><?php echo date('jS F', strtotime($key->created_at)); ?></a>
		    	</li>
		    </ul>
		    <p class="card-text"><a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>"><i class="fa fa-map-marker"></i> <?php echo $key->subCountyName; ?> (Views <?php echo $key->views; ?>)</a></p>
        <p class="card-text"><a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>"><i class="fa fa-user"></i> <?php echo $key->firstName; ?> <?php echo $key->lastName; ?></a></p>
		    <div class="product-ratings">
		    	<a href="<?php $url = URL::to("/viewRent"."/".$key->id); print_r($url); ?>" class="btn btn-primary btn-block">Ksh. <?php echo number_format($key->amount,0); ?></a>
		    </div>
		</div>
	</div>
</div>
</div>

<?php } ?>
<?php } else { ?>
  <div class="alert alert-danger" style="margin:0 auto;">
      <p style="color:#000000 !important;">Your search criteria did not return any rentals. Please try with different selections.</p>
  </div>
<?php } ?>


		</div>
	</div>
</section>



<!--==========================================
=            All Category Section            =
===========================================-->

@include('frontend.allcategories')

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')
@include('frontend.footerlinks')
<script type="text/javascript">
       $.ajax({
           url: "{{ route('getRentalTypes') }}?categoryId=<?php echo $categoryDetails->id ?>",
           method: 'GET',
           success: function(data) {
               $('#propertytypes').html(data.html);
           }
       });
       $("#countyId").change(function(){
           $.ajax({
               url: "{{ route('getSubcounties') }}?countyId=" + $(this).val(),
               method: 'GET',
               success: function(data) {
                   $('#subCountyId').html(data.html);
               }
           });
       });
</script>
</body>

</html>
