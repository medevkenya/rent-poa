<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashboard</title>

@include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.topinner')

<!--==================================
=            User Profile            =
===================================-->

<section class="dashboard section">
	<!-- Container Start -->
	<div class="container">
		<!-- Row Start -->
		<div class="row">



			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Recently Favorited -->
				<div class="widget dashboard-container my-adslist">
					<h3 class="widget-header">My Ads</h3>
					<table class="table table-responsive product-dashboard-table">
						<thead>
							<tr>
								<th>Image</th>
								<th>Title</th>
								<th class="text-center">Category</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>

              <?php
                if(!isset($data) || $data == null || empty($data) || count($data) == 0) { ?>
                  <div class="alert alert-danger">
                      <p style="text-align:center;">Hi <strong>{{{ Auth::user()->firstName }}} {{{ Auth::user()->lastName }}}</strong>, you do not have not upload any rentals yet. Click on the button below to upload.</p>
                      <center><p><a href="{{URL::to('/addListing')}}" class="btn btn-main">Upload New Rental</a></p></center>
                  </div>
                <?php } ?>

              <?php foreach ($data as $key) {
                if($key->status == 0) {
                  $status = "Inactive";
                }
                else {
                  $status = "Active";
                }

                $photoDetails = App\Propertyphotos::where('propertyId',$key->id)->orderBy('id','DESC')->limit(1)->first();
                if($photoDetails) {
                $featuredPhoto = $photoDetails->photo;
                }
                else {
                  $featuredPhoto = "rental.jpg";
                }
                ?>
							<tr>
								<td class="product-thumb">
									<img width="80px" height="auto" src="{{ URL::to('/') }}/allphotos/<?php echo $featuredPhoto; ?>" alt="RentPoa"></td>
								<td class="product-details">
									<h3 class="title"><?php echo $key->title; ?></h3>
									<span class="add-id"><strong>Rental ID:</strong> <?php echo $key->pid; ?></span>
									<span><strong>Posted on: </strong><time><?php echo date("Y-m-d H:i A", strtotime($key->created_at)); ?></time> </span>
									<span class="status active"><strong>Status</strong><?php echo $status; ?></span>
									<span class="location"><strong>Views</strong> (<?php echo $key->views; ?>)</span>
								</td>
								<td class="product-category"><span class="categories"><?php echo $key->propertytypeName; ?></span></td>
								<td class="action" data-title="Action">

									<div class="">

										<ul class="list-inline justify-content-center">
											<li class="list-inline-item">
												<a data-toggle="tooltip" data-placement="top" title="View Rental" class="view" href="<?php $url = URL::to("/viewRental"."/".$key->id); print_r($url); ?>">
													<i class="fa fa-eye"></i>
												</a>
											</li>
											<li class="list-inline-item">
												<a class="edit" data-toggle="tooltip" data-placement="top" title="Edit Rental" href="<?php $url = URL::to("/editRental"."/".$key->id); print_r($url); ?>">
													<i class="fa fa-pencil"></i>
												</a>
											</li>
											<li class="list-inline-item">
												<a class="delete" data-toggle="tooltip" data-placement="top" title="Remove Rental" href="<?php $url = URL::to("/removeRental"."/".$key->id); print_r($url); ?>" onclick="return confirm('Are you sure you want to remove this rental?');">
													<i class="fa fa-trash"></i>
												</a>
											</li>
										</ul>
									</div>
								</td>
							</tr>

            <?php } ?>

						</tbody>
					</table>

          {{ $data->links( "pagination::bootstrap-4") }}

				</div>
			</div>

  @include('frontend.rightbarinner')

		</div>
		<!-- Row End -->
	</div>
	<!-- Container End -->
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

</body>

</html>
