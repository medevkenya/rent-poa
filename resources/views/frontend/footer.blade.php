<footer class="footer section section-sm">
  <!-- Container Start -->
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-7 offset-md-1 offset-lg-0">
        <!-- About -->
        <div class="block about">
          <!-- footer logo -->
          <img src="{{ URL::asset('images/logo.png')}}" alt="RentPoa" style="width:60%;">
          <!-- description -->
          <p class="alt-color">Get RentPoa without much hustle. Simplified for you.</p>
        </div>
      </div>
      <!-- Link list -->
      <div class="col-lg-2 offset-lg-1 col-md-3">
        <div class="block">
          <h4>Quick Links</h4>
          <ul>
            <li><a href="{{URL::to('/')}}">Home</a></li>
            <li><a href="{{URL::to('/termsOfService')}}">Terms of Services</a></li>
          </ul>
        </div>
      </div>
      <!-- Link list -->
      <div class="col-lg-2 col-md-3 offset-md-1 offset-lg-0">
        <div class="block">
          <h4>Deals & Offers</h4>
          <ul>
            <li><a href="{{URL::to('/latestRentals')}}">Latest Rentals</a></li>
            <li><a href="{{URL::to('/popularRentals')}}">Most Viewied</a></li>
          </ul>
        </div>
      </div>
      <!-- Promotion -->
      <div class="col-lg-4 col-md-7">
        <!-- App promotion -->
        <div class="block-2 app-promotion">
          <a href="">
            <!-- Icon -->
            <img src="{{ URL::asset('assets/images/footer/phone-icon.png')}}" alt="mobile-icon">
          </a>
          <p>The Best Rentals Near You</p>
        </div>
      </div>
    </div>
  </div>
  <!-- Container End -->
</footer>
