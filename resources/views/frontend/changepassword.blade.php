<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Change Password</title>

  @include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.topinner')

<!--==================================
=            User Profile            =
===================================-->

<section class="user-profile section">
	<div class="container">
		<div class="row">

			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Edit Personal Info -->
				<div class="widget personal-info">
          <h2>Change Password</h2>

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
          @endif

            {!! Form::open(['url'=>'dochangepassword']) !!}
            {{ csrf_field() }}
            <div class="row">

            <div class="form-group  col-md-12">
						    <label for="first-name">Current Password</label>
						    <input type="password" class="form-control" name="oldpassword" id="oldpassword" required>
                @if ($errors->has('oldpassword'))
                    <span class="text-danger">{{ $errors->first('oldpassword') }}</span>
                @endif
						</div>

            <div class="form-group  col-md-12">
						    <label for="first-name">New Password</label>
						    <input type="password" class="form-control" name="password" id="password" required>
                @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
						</div>

            <div class="form-group  col-md-12">
						    <label for="first-name">Confirm Password</label>
						    <input type="password" class="form-control" name="cpassword" id="cpassword" required>
                @if ($errors->has('cpassword'))
                    <span class="text-danger">{{ $errors->first('cpassword') }}</span>
                @endif
						</div>

          </div>
						<!-- Submit button -->
						<button class="btn btn-main" type="submit">Save Changes</button>
					{!! Form::close() !!}
				</div>

			</div>

      @include('frontend.rightbarinner')

		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

@include('frontend.footerlinks')

</body>

</html>
