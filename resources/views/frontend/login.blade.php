<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>

@include('frontend.headerlinks')

</head>

<body class="body-wrapper">

@include('frontend.top')

<!--==================================
=            User Profile            =
===================================-->

<section class="user-profile section">
	<div class="container">
		<div class="row">

			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Edit Personal Info -->
				<div class="widget personal-info">
          <h2>Login</h2>
					<h3 class="widget-header user">Login to upload your properties</h3>

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
          @endif

          @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
          @endif

            {!! Form::open(['url'=>'doLogin']) !!}
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
						    <label for="comunity-name">Email Address</label>
						    <input type="email" class="form-control" name="email" id="email"  value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
						</div>

            <div class="form-group">
						    <label for="current-password {{ $errors->has('password') ? 'has-error' : '' }}">Password</label>
						    <input type="password" name="password" class="form-control" id="password" required>
                @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
						</div>

						<div class="form-check">
						     <a href="{{URL::to('/forgotPassword')}}" class="linkref">Recover forgotten password</a>
						</div>

						<!-- Submit button -->
						<button class="btn btn-main" type="submit">Login</button>
					{!! Form::close() !!}
          <br>
          <div class="form-check">
               <a href="{{URL::to('/register')}}" class="linkref">Create New Account</a>
          </div>
				</div>

			</div>

      @include('frontend.rightbar')

		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

@include('frontend.footer')
<!-- Footer Bottom -->
@include('frontend.footerbottom')

</body>

</html>
