<!-- Footer -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="p30-15" style="padding: 50px 30px;" bgcolor="#ffffff">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" style="padding-bottom: 30px;">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;">
															<a href="https://www.facebook.com/RentPoa/" target="_blank">
															<img src="{{ URL::to('/') }}/emails/images/t8_ico_facebook.jpg" width="38" height="38" border="0" alt="" />
															</a></td>
															<td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;">
															<a href="https://twitter.com/RentPoa" target="_blank">
															<img src="{{ URL::to('/') }}/emails/images/t8_ico_twitter.jpg" width="38" height="38" border="0" alt="" />
															</a></td>
															<td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;">
															<a href="https://www.instagram.com/RentPoa" target="_blank">
															<img src="{{ URL::to('/') }}/emails/images/t8_ico_instagram.jpg" width="38" height="38" border="0" alt="" />
															</a></td>
															<td class="img" width="38" style="font-size:0pt; line-height:0pt; text-align:left;">
															<a href="#" target="_blank">
															<img src="{{ URL::to('/') }}/emails/images/t8_ico_linkedin.jpg" width="38" height="38" border="0" alt="" />
															</a></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="text-footer1 pb10" style="color:#999999; font-family:'Noto Sans', Arial,sans-serif; font-size:16px; line-height:20px; text-align:center; padding-bottom:10px;">Rent Poa - Best rentals near you!</td>
											</tr>
											<tr>
												<td class="text-footer2 pb30" style="color:#999999; font-family:'Noto Sans', Arial,sans-serif; font-size:12px; line-height:26px; text-align:center; padding-bottom:30px;">Simplified for you</td>
											</tr>
											 <tr>
												<td class="text-footer3" style="color:#c0c0c0; font-family:'Noto Sans', Arial,sans-serif; font-size:12px; line-height:18px; text-align:center;">
													<span class="link3-u" style="color:#c0c0c0;">Phone : +254702066508</span>
													Email : <a href="#" target="_blank" class="link3-u" style="color:#c0c0c0;">
														<span class="link3-u" style="color:#c0c0c0; text-decoration:underline;">info@rentpoa.com</span></a>
													</td>
											</tr>
											<!-- <tr>
												<td class="text-footer3" style="color:#c0c0c0; font-family:'Noto Sans', Arial,sans-serif; font-size:12px; line-height:18px; text-align:center;"><a href="#" target="_blank" class="link3-u" style="color:#c0c0c0; text-decoration:underline;"><span class="link3-u" style="color:#c0c0c0; text-decoration:underline;">Unsubscribe</span></a> from this mailing list.</td>
											</tr> -->
										</table>
									</td>
								</tr>
							</table>
							<!-- END Footer -->
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
