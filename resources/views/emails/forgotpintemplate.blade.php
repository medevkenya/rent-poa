<table style="background: rgb(0,51,102);
background: linear-gradient(90deg, rgba(0,51,102,1) 0%, rgba(167,69,36,1) 50%);
color: #ffffff;width:100%;border-radius:4px;padding:2%;">
<img src="{{ URL::to('/') }}/images/logo.png" style="width:15%;"/>
<p style="color:#fff;">
Dear {{ $name }},
</p>
<p style="color:#fff;">
Click on the link below to reset your Account Password.
</p>

<a href="{{ url('/') }}/resetPasswordLink/{{ $pass }}/{{ $email }}" style="text-decoration:none;background-color: #225253;
  border: 1px solid #225253;
  color: white;
  padding: 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;border-radius:6px;
  margin: 4px 2px;"><b style="color:#fff;">CLICK TO RESET PASSWORD</b></a>

<hr>
<p style="color:#fff;">
Thank you for your business.
</p>
<p style="color:#fff;">Rent Poa</p>
<p style="color:#fff;">All emails sent to or from Rent Poa are subject to our Terms & Conditions of use.</p>
<p style="color:#fff;">&copy; All rights reserved</p>
</table>
