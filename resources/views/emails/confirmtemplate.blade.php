<table style="background: rgb(0,51,102);
background: linear-gradient(90deg, rgba(0,51,102,1) 0%, rgba(167,69,36,1) 50%);
color: #ffffff;width:100%;border-radius:4px;">
<img src="{{ URL::to('/') }}/images/logo.png" style="width:15%;"/>
<p style="color:#fff;">
Dear {{ $name }},
</p>
<p style="color:#fff;">
Your new Rent Poa account password has been set successfully.
</p>
<hr>
<p style="color:#fff;">
Thank you for your business.
</p>
<p style="color:#fff;">Rent Poa</p>
<p style="color:#fff;">All emails sent to or from Rent Poa are subject to our Terms & Conditions of use.</p>
<p style="color:#fff;">&copy; All rights reserved</p>
</table>
